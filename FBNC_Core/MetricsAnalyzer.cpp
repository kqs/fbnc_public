#include "MetricsAnalyzer.h"
#include "Utils.h"
#include <omp.h>

MetricsAnalyzer::MetricsAnalyzer() {}

std::vector<float> MetricsAnalyzer::bellmanFord(const std::vector<std::vector<float>> network, int source)
{
	auto size = network.size();

	std::vector<float> distances(size, FLT_MAX);
	distances[source] = 0;

	for (auto i = 1; i <= size - 1; ++i)
	{
		#pragma omp parallel for
		for (auto x1 = 0; x1 < size; ++x1)
		{
			for (auto x2 = 0; x2 < size; ++x2)
			{
				auto weight = network[x1][x2];

				if (weight == 0) continue;

				if (distances[x2] > distances[x1] + weight && distances[x1] != FLT_MAX)
					distances[x2] = distances[x1] + weight;
			}
		}
	}

	//for (auto x1 = 0; x1 < size; ++x1)
	//{
	//	for (auto x2 = 0; x2 < size; ++x2)
	//	{
	//		auto weight = network[x1][x2];

	//		if (weight == 0) continue;

	//		auto dist1 = distances[x1];
	//		auto dist2 = distances[x2];
	//		if (distances[x2] > distances[x1] + weight) {
	//			std::cout << "Error: Got negative cycles!" << std::endl;
	//			return std::vector<float>();
	//		}
	//	}
	//}

	for (auto i = 0; i < size; ++i)
	{
		if (distances[i] == FLT_MAX)
			distances[i] = 0;
	}

	return distances;
}

float MetricsAnalyzer::computeAveragePathLength(std::vector<std::vector<float>> network)
{
	float shortest_path_sum = 0;

	//std::ofstream f1("tata.txt");
	for (auto i = 0; i < 128; ++i)
	{
		if (i == 6) continue;

		auto distances = bellmanFord(network, i);

		for (auto j = 0; j < distances.size(); ++j)
		{
			shortest_path_sum += static_cast<float>(distances[j]);
		}
	}

	std::cout << "Shortest path sum: " << shortest_path_sum << std::endl;
	return (1.0f / (network.size()*(network.size()-1))) * shortest_path_sum;
}

array<float>^ MetricsAnalyzer::GetAveragePathLength(array<System::String^> ^inputFilePaths)
{
	array<float>^ avgPathLengths = gcnew array<float>(inputFilePaths->Length);

	int i = 0;
	for each (System::String^ inputFile in inputFilePaths)
	{
		std::string file;
		MarshalString(inputFile, file);

		std::vector<std::vector<float>> network = ReadMatrixFromFile(file);
		float avgPathLength = computeAveragePathLength(network);

		avgPathLengths[i] = avgPathLength;
		i++;
	}

	return avgPathLengths;
}