f1 = fscanf(fopen('s1.txt'), '%f');
f2 = fscanf(fopen('s2.txt'), '%f');
f3 = fscanf(fopen('cross.txt'), '%f');
f4 = fscanf(fopen('scaled.txt'), '%f');

subplot(4, 1, 1);
plot(f1);
subplot(4, 1, 2);
plot(f2);
subplot(4, 1, 3);
plot(f3);
subplot(4, 1, 4);
plot(f4);