#ifndef USED_MYMATH
#define USED_MYMATH
#include "Math.h"


__forceinline int round(float x) 
{
	 return (int)((x > 0.0) ? floor(x + 0.5) : ceil(x - 0.5));
}

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#endif