#include "Cross_Correlation.h"
#include <iostream>
#include <omp.h>
#include <chrono>
#include <algorithm>

std::vector<float> Cross_Correlation::Compute(std::vector<float> first_signal, std::vector<float> second_signal)
{
    int correlation_signal_size = 2 * first_signal.size() - 1;
    int signal_size = first_signal.size();

    std::vector<float> correlation_signal(correlation_signal_size);

    omp_set_num_threads(8);

    auto t = time(0);   // get time now
    auto now = localtime(&t);
    std::cout << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << std::endl;

    float correlation = 0;
    // negative lagging
    for (auto negative_lag = -signal_size + 1; negative_lag <= 0; ++negative_lag)
    {
        //std::cout << "[negative] Doing for lag :" << negative_lag << std::endl;
        correlation = 0;
        for (auto lagged_signal_pos = std::abs(negative_lag); lagged_signal_pos < signal_size; ++lagged_signal_pos)
        {
            correlation += first_signal[lagged_signal_pos + negative_lag] * second_signal[lagged_signal_pos];
        }
        correlation_signal[negative_lag + signal_size - 1] = correlation;
        /*std::cout << "LAG: " << negative_lag << " - Correlation: " << correlation << std::endl;*/
    }

    std::cout << "Negative lag done..." << std::endl;

    // positive lag
    for (auto positive_lag = 1; positive_lag < signal_size; ++positive_lag)
    {
        correlation = 0;
        //std::cout << "[positive] Doing for lag :" << positive_lag << std::endl;
        for (auto lagged_signal_pos = positive_lag; lagged_signal_pos < signal_size; ++lagged_signal_pos)
        {
            correlation += first_signal[lagged_signal_pos] * second_signal[lagged_signal_pos - positive_lag];
        }

        correlation_signal[signal_size + positive_lag - 1 ] = correlation;
        /*std::cout << "LAG: " << positive_lag << " - Correlation: " << correlation << std::endl;*/
    }

    std::cout << "Positive lag done..." << std::endl;

    t = time(0);
    now = localtime(&t);
    std::cout << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << std::endl;

    return correlation_signal;
}

int Cross_Correlation::get_triggerring_signal(std::vector<float> &correlation, long *delay, float *peak)
{
    // if lag delay is > 0 (positive) - second signal is determining first one (second signal is before first signal hence we have to INCREASE its position)
    // if lag delay is < 0 (negative) - first signal is determining first one (second signal is after first signal hence we have to DECREASE its position)

    float max = 0.0f;
    size_t maxPos = -99999999;
    for (size_t i = 0; i < correlation.size(); ++i)
    {
        if (abs(correlation[i]) > abs(max))
        {
            max = correlation[i];
            maxPos = i;
        }
    }

	*peak = max;
	*delay = maxPos;

    if (maxPos > 0)
    {
        // second signal leads first signal
        return 2;
	}
	if (maxPos < 0) {
		return 1;
	}
	return -1;
}
