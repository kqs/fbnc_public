#include <vector>

class Cross_Correlation
{
public: 
    std::vector<float> Compute(std::vector<float> first_signal, std::vector<float> second_signal);
    static int get_triggerring_signal(std::vector<float> &correlation, long *lag, float *peak);
};
