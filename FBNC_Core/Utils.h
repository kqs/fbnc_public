#pragma once

#include <vector>
#include <string>

#define CHANNELS_NB 128
#define FREQUENCY 512

#define THREADS_NB 8

enum FunctionalNetworkType
{
	LAGS_WEIGHTED,
	PEAKS_WEIGHTED,
	PEARSON_WEIGHTED
};

enum CorrelationType
{
	CROSS_CORRELATION,
	SCALED_CORRELATION,
	PEARSON_CORRELATION
};

public enum class CorrelationTypeBridge
{
	CROSS_CORRELATION,
	SCALED_CORRELATION,
	PEARSON_CORRELATION
};

struct edge
{
	float _weight;
	int _from;
	int _to;
	float _jaccardIndex;

	edge(int from, int to, float weight, float jaccardIndex) : _weight(weight), _from(from), _to(to), _jaccardIndex(jaccardIndex) {}

	bool operator< (const edge str) const
	{
		return (_jaccardIndex > str._jaccardIndex);
	}
};

extern std::vector<int> OCCIPITAL;
extern std::vector<int> FRONTAL;
extern std::vector<int> PARIETAL;
extern std::vector<int> LEFT_TEMPORAL;
extern std::vector<int> RIGHT_TEMPORAL;

extern struct BrainAreas {
	std::vector<std::vector<float>> occipital_frontal;
	std::vector<std::vector<float>> occipital_parietal;
	std::vector<std::vector<float>> occipital_leftTemporal;
	std::vector<std::vector<float>> occipital_rightTemporal;
	std::vector<std::vector<float>> frontal_parietal;
	std::vector<std::vector<float>> frontal_leftTemporal;
	std::vector<std::vector<float>> frontal_rightTemporal;
	std::vector<std::vector<float>> parietal_leftTemporal;
	std::vector<std::vector<float>> parietal_rightTemporal;
	std::vector<std::vector<float>> leftTemporal_rightTemporal;
};

public delegate void ProgressCallback(System::String^ str);

int GetMS(int samples);
int GetSamples(int ms);
std::string FormatCurrentExecution(std::string subjectName, CorrelationType corrType, int eventsInterval[3]);
std::string FormatInputEvents(int events[3]);
std::string ToString(CorrelationType ct);
std::vector<std::vector<float>> ReadMatrixFromFile(std::string fileName);
void WriteMatrixToFile(std::vector<std::vector<float>> matrix, std::string matrixFileNamepath);
void WriteVectorToFile(std::vector<float> matrix, std::string vectorFileNamepath);

template <class T> std::vector<T> GetSamplesByRange(
	std::vector<T> samples,
	int leftPosition,
	int rightPosition)
{
	auto start = samples.begin() + leftPosition;
	auto ending = samples.begin() + rightPosition;

	return std::vector<T>(start, ending);
}

void MarshalString(System::String^ s, std::string& os);