#pragma once

#include "Utils.h"

public ref class FunctionalNetworkCreatorBridge
{
public:
	FunctionalNetworkCreatorBridge(
		System::String^ binDataPath, 
		System::String^ eventCodesPath, 
		System::String^ eventTimestampsPath, 
		System::String^ outputPath, 
		System::String^ subjectName,
		ProgressCallback^ progressCallback);
	void Generate(CorrelationTypeBridge corrType, int leftBound, int middleBound, int rightBound);
private:
	void* _creator;
	void(*_progressCallback)(std::string);
	ProgressCallback^ _progressDelegate;
};

