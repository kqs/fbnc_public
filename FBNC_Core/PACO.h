#pragma once

#include<vector>
#include "Utils.h"
#include<iostream>

class PACO
{
private:
	

	static std::vector<int> findNeighbors(std::vector<std::vector<float>> matrix, int node);
	static std::vector<edge> getEdgesSortedByJaccard(std::vector<std::vector<float>> matrix);
	static int getCommunity(std::vector<std::vector<int>> communities, int node);
	static void migrateToCommunity(std::vector<std::vector<int>> &communities, int node, int newCommunityIndex, int oldCommunityIndex);
	static double getNumberOfEdges(std::vector<std::vector<float>> matrix);
	static double KL(double q, double p);
	static double ASYMPTOTIC_SURPRISE(std::vector<std::vector<float>> matrix, std::vector<std::vector<int>> communities);

public:
	PACO();

	static std::vector<std::vector<std::vector<float>>> getCommunitiesGraphs(std::vector<std::vector<float>> graph, std::vector<std::vector<int>> communities);
	static std::vector<std::vector<int>> computePACO(std::vector<std::vector<float>> matrix);
};

