#include "FunctionalNetworkCreator.h"
#include "libs\correlation_lib\CrossCorrelation-CC.h"
#include "libs\correlation_lib\ScaledCorrelation-CC.h"

#include <string>
#include <algorithm>
#include <time.h>
#include <omp.h>

FunctionalNetworkCreator::FunctionalNetworkCreator(
	std::string binDataPath, 
	std::string eventCodesPath, 
	std::string eventTimestampsPath, 
	std::string outputPath, 
	std::string subjectName, 
	void(*progressCallback)(std::string))
: _outputPath(outputPath)
, _subjectName(subjectName)
, _progressCallback(progressCallback)
{
	omp_set_num_threads(THREADS_NB);

	std::cout << "Initializing creator..." << std::endl;
	_progressCallback("Initializing creator...");
	fileProcessor = new EEGFileProcessor(binDataPath, eventCodesPath, eventTimestampsPath, progressCallback);
}

void allocateMatrixSize(std::vector<std::vector<float>> &matrix, int rows, int cols)
{
	matrix.resize(rows);
	for (auto i = 0; i < rows; ++i)
		matrix[i].resize(cols);
}

void FunctionalNetworkCreator::Generate(CorrelationType corrType, int eventsInterval[3])
{
	// network_peaks/s1/CC/
	std::string peaksNetworkOutputPath(_outputPath + "/network_peaks/" + _subjectName + "/" + ToString(corrType));
	std::string lagsNetworkOutputFormat(_outputPath + "/network_lags/" + _subjectName + "/" + ToString(corrType));
	std::string pearsonNetworkOutputFormat(_outputPath + "/network_pearson/" + _subjectName);
	// network_peaks/s1/CC/129_1
	std::string peaksNetworkOutputEventsPath(peaksNetworkOutputPath + "/" + FormatInputEvents(eventsInterval));
	std::string lagsNetworkOutputEventsPath(lagsNetworkOutputFormat + "/" + FormatInputEvents(eventsInterval));
	std::string pearsonNetworkOutputEventsPath(pearsonNetworkOutputFormat + "/" + FormatInputEvents(eventsInterval));

	System::IO::Directory::CreateDirectory(gcnew System::String(peaksNetworkOutputEventsPath.c_str()));
	System::IO::Directory::CreateDirectory(gcnew System::String(lagsNetworkOutputEventsPath.c_str()));
	System::IO::Directory::CreateDirectory(gcnew System::String(pearsonNetworkOutputEventsPath.c_str()));

	std::string execFormat = FormatCurrentExecution(_subjectName, corrType, eventsInterval);
	std::cout << execFormat << " Parsing events...." << std::endl;
	_progressCallback(execFormat + " Parsing events...");
	time_t t0 = time(NULL);
	auto events = fileProcessor->GetEventsBinaryData<float>(eventsInterval[0], eventsInterval[1], eventsInterval[2]);
	time_t t1 = time(NULL);

	std::cout << execFormat << " Parsing events...." << std::endl;
	std::cout << execFormat << " Found [" << std::to_string(events.size()) << "] events..." << std::endl;
	std::cout << execFormat << " Starting events processing..." << std::endl;
	_progressCallback(std::string(execFormat + " Parsing events....DONE"));
	_progressCallback(std::string(execFormat + " Found [" + std::to_string(events.size()) + "] events..."));
	_progressCallback(std::string(execFormat + " Starting events processing..."));

	std::vector<FunctionalNetwork> peaksMatrices;
	std::vector<FunctionalNetwork> lagsMatrices;
	std::vector<FunctionalNetwork> pearsonMatrices;
	peaksMatrices.resize(events.size());
	lagsMatrices .resize(events.size());
	pearsonMatrices.resize(events.size());

	#pragma omp parallel for
	for (int i = 0; i < events.size(); ++i)
	{
		auto networks = generateNetwork(events[i], corrType);

		if (corrType == CorrelationType::PEARSON_CORRELATION) {
			WriteMatrixToFile(networks[0].Content, std::string(pearsonNetworkOutputEventsPath + "/network_pearson_" + _subjectName + "_" + ToString(corrType) + "_" + FormatInputEvents(eventsInterval) + "_trial_" + std::to_string(i) + ".txt"));

			pearsonMatrices[i] = networks[0];
		}
		else {
			WriteMatrixToFile(networks[0].Content, std::string(peaksNetworkOutputEventsPath + "/network_peaks_" + _subjectName + "_" + ToString(corrType) + "_" + FormatInputEvents(eventsInterval) + "_trial_" + std::to_string(i) + ".txt"));
			WriteMatrixToFile(networks[1].Content, std::string(lagsNetworkOutputEventsPath + "/network_lags_" + _subjectName + "_" + ToString(corrType) + "_" + FormatInputEvents(eventsInterval) + "_trial_" + std::to_string(i) + ".txt"));
			WriteMatrixToFile(networks[2].Content, std::string(peaksNetworkOutputEventsPath + "/correlation" + _subjectName + "_" + ToString(corrType) + "_" + FormatInputEvents(eventsInterval) + "_trial_" + std::to_string(i) + ".txt"));

			peaksMatrices[i] = networks[0];
			lagsMatrices[i] = networks[1];
		}
	}

	// Generating means
	std::cout << execFormat << " Creating averages + printing areas..." << std::endl;
	_progressCallback(std::string(execFormat + " Creating averages + printing areas..."));

	if (corrType == CorrelationType::PEARSON_CORRELATION)
	{
		FunctionalNetwork meanPearsonMatrix(getMeanMatrix(pearsonMatrices), FunctionalNetworkType::PEARSON_WEIGHTED, corrType);
		WriteMatrixToFile(meanPearsonMatrix.Content, std::string(pearsonNetworkOutputEventsPath + "/average.txt"));

		meanPearsonMatrix.PrintAreas(pearsonNetworkOutputEventsPath);
	}
	else {
		FunctionalNetwork meanPeaksMatrix(getMeanMatrix(peaksMatrices), FunctionalNetworkType::PEAKS_WEIGHTED, corrType);
		FunctionalNetwork meanLagsMatrix(getMeanMatrix(lagsMatrices), FunctionalNetworkType::LAGS_WEIGHTED, corrType);

		WriteMatrixToFile(meanPeaksMatrix.Content, std::string(peaksNetworkOutputEventsPath + "/average.txt"));
		WriteMatrixToFile(meanLagsMatrix.Content, std::string(lagsNetworkOutputEventsPath + "/average.txt"));

		// Generating + Printing areas
		meanPeaksMatrix.PrintAreas(peaksNetworkOutputEventsPath);
		meanLagsMatrix.PrintAreas(lagsNetworkOutputEventsPath);
	}

	std::cout << execFormat << " DONE" << std::endl;
	_progressCallback(std::string(execFormat + " DONE"));
}

std::vector<std::vector<float>> FunctionalNetworkCreator::getMeanMatrix(std::vector<FunctionalNetwork> matrices)
{
	std::vector<std::vector<float>> meanMatrix;
	allocateMatrixSize(meanMatrix, CHANNELS_NB, CHANNELS_NB);

	for (size_t i = 0; i < meanMatrix.size(); ++i)
	{
		for (size_t j = 0; j < meanMatrix[i].size(); ++j)
		{
			float sum = 0.0f;
			for (size_t k = 0; k < matrices.size(); ++k)
				sum += matrices[k].Content[i][j];
			meanMatrix[i][j] = (float)sum / matrices.size();
		}
	}

	return meanMatrix;
}

std::vector<FunctionalNetwork> FunctionalNetworkCreator::generateNetwork(std::vector<std::vector<float>> &channelsEventBinaryData, CorrelationType corrType)
{
	std::vector<std::vector<float>> peaksWeightedMatrix, lagsWeightedMatrix, pearsonWeightedMatrix, correlationData;

	// for 512Hz
	//auto windowSize = GetSamples(100);
	//auto s = GetSamples(25);

	// for 1kHz
	int windowSize = 100;
	int s = 25;

	allocateMatrixSize(peaksWeightedMatrix, CHANNELS_NB, CHANNELS_NB);
	allocateMatrixSize(lagsWeightedMatrix, CHANNELS_NB, CHANNELS_NB);
	allocateMatrixSize(pearsonWeightedMatrix, CHANNELS_NB, CHANNELS_NB);

	#pragma omp parallel for
	for (auto i = 0; i < CHANNELS_NB; ++i)
	{
		for (auto j = i + 1; j < CHANNELS_NB; ++j)
		{
			if (i == j
				|| j == CHANNELS_NB) continue;
			if (_subjectName == "s1" && (i == 6 || j == 6)) continue;
			if (_subjectName == "s2" && (i == 35 || j == 35 || i == 95 || j == 95)) continue;

			auto iError = 0;
			auto trialLength = channelsEventBinaryData[i].size();

			float *correlogram;
			std::vector<float> correlation;

			CCrossCorrelationCC ComputeClassicCC(windowSize, trialLength, iError);
			CScaledCorrelationCC ComputeScaledCC(s, windowSize, trialLength, iError);

			if (iError == 0)
			{
				switch(corrType)
				{
					case CorrelationType::CROSS_CORRELATION:
						// Compute Cross-Correlation (non-normalized)
						ComputeClassicCC.ComputeCrossCorrelation(&channelsEventBinaryData[i][0], channelsEventBinaryData[i].size(), &channelsEventBinaryData[j][0], channelsEventBinaryData[j].size(), 0);
						correlogram = ComputeClassicCC.GetCrossCorrelogram();
						break;
					case CorrelationType::SCALED_CORRELATION:
						// Compute Scaled-Correlation
						ComputeScaledCC.ComputeScaledCorrelation(&channelsEventBinaryData[i][0], channelsEventBinaryData[i].size(), &channelsEventBinaryData[j][0], channelsEventBinaryData[j].size(), 0);
						correlogram = ComputeScaledCC.GetScaledCrossCorrelogram();
						break;
					case CorrelationType::PEARSON_CORRELATION:
						float pearsonCoefficient = ComputeScaledCC.ComputePearsonCorrelation(&channelsEventBinaryData[i][0], channelsEventBinaryData[i].size(), &channelsEventBinaryData[j][0], channelsEventBinaryData[j].size());
						
						pearsonWeightedMatrix[i][j] = abs(pearsonCoefficient);
						pearsonWeightedMatrix[j][i] = abs(pearsonCoefficient);
						continue;
						break;
				}


				for (auto k = 0; k < 2 * windowSize + 1; ++k)
				{
					if (correlogram[k] != NAN)
						correlation.push_back(correlogram[k]);
				}

				long corrLag = 0;
				float corrPeak = 0.0f;
				
				getLagAndPeak(correlation, &corrLag, &corrPeak);

				if (corrPeak != 0)
					corrLag = corrLag - windowSize;
				//corrLag = GetMS(corrLag);

				correlationData.push_back(correlation);

				// i leads j
				if (corrLag < 0)
				{
					peaksWeightedMatrix[i][j] = abs(corrPeak);
					lagsWeightedMatrix[i][j] = abs(corrLag);
				}
				// j leads i
				if (corrLag > 0) 
				{
					peaksWeightedMatrix[j][i] = abs(corrPeak);
					lagsWeightedMatrix[j][i] = abs(corrLag);
				}
				if (corrLag == 0)
				{
					peaksWeightedMatrix[i][j] = abs(corrPeak);
					peaksWeightedMatrix[j][i] = abs(corrPeak);
					
					lagsWeightedMatrix[i][j] = abs(corrLag);
					lagsWeightedMatrix[j][i] = abs(corrLag);
				}
			}
		}
	}

	std::vector<FunctionalNetwork> functionalNetworks;
	if (corrType == CorrelationType::PEARSON_CORRELATION)
		functionalNetworks.push_back(FunctionalNetwork(pearsonWeightedMatrix, FunctionalNetworkType::PEARSON_WEIGHTED, corrType));
	else {
		functionalNetworks.push_back(FunctionalNetwork(peaksWeightedMatrix, FunctionalNetworkType::PEAKS_WEIGHTED, corrType));
		functionalNetworks.push_back(FunctionalNetwork(lagsWeightedMatrix, FunctionalNetworkType::LAGS_WEIGHTED, corrType));

		functionalNetworks.push_back(FunctionalNetwork(correlationData, FunctionalNetworkType::LAGS_WEIGHTED, corrType));
	}

	return functionalNetworks;
}

void computeIQR(std::vector<float> vec, float *q1, float *q2, float *q3, float *iqr)
{
	auto vecSize = vec.size();
	auto halfVecsize = vecSize / 2;

	//std::cout << "vec size: " << vecSize << std::endl;
	//std::cout << "half vec size : " << halfVecsize << std::endl;

	if (vecSize % 2 == 0)
	{
		/*std::cout << "even nbs..." << std::endl;*/
		*q2 = (float)(vec.at(halfVecsize - 1) + vec.at(halfVecsize)) / 2;
	}
	else
	{
		/*std::cout << "odd nbs..." << std::endl;*/
		*q2 = vec.at(halfVecsize);
	}

	if (halfVecsize % 2 == 0)
	{
		*q1 = (vec.at(vecSize * 1 / 4) + vec.at(vecSize * 1 / 4 - 1)) / 2;
		*q3 = (vec.at(vecSize * 3 / 4) + vec.at(vecSize * 3 / 4 - 1)) / 2;

		*iqr = *q3 - *q1;
	}
	else if (halfVecsize % 2 != 0)
	{
		*q1 = vec.at(vecSize * 1 / 4);
		*q3 = vec.at(vecSize * 3 / 4);
		*iqr = *q3 - *q1;
	}
}

void FunctionalNetworkCreator::getLagAndPeak(std::vector<float> &correlation, long *delay, float *peak)
{
	// if lag delay is > 0 (positive) - second signal is determining first one (second signal is before first signal hence we have to INCREASE its position)
	// if lag delay is < 0 (negative) - first signal is determining first one (second signal is after first signal hence we have to DECREASE its position)

	float max = 0.0f;
	size_t maxPos = -99999999;
	float mean = 0;
	for (size_t i = 0; i < correlation.size(); ++i)
	{
		mean += correlation[i];

		if (abs(correlation[i]) > abs(max))
		{
			max = correlation[i];
			maxPos = i;
		}
	}

	float q1, q2, q3, iqr;
	std::sort(correlation.begin(), correlation.end());
	computeIQR(correlation, &q1, &q2, &q3, &iqr);

	float x = abs((max - q2) / iqr);

	if (x >= 2) {
		*peak = max;
		*delay = maxPos;
	}
	else {
		*peak = 0;
		*delay = 0;
	}

	
}