#pragma once

#include "Utils.h"
#include <iostream>
#include <vector>

class FunctionalNetwork
{
public:
	FunctionalNetwork();
	FunctionalNetwork(std::vector<std::vector<float>> dataContent, FunctionalNetworkType networkType, CorrelationType corrType);
	BrainAreas ParseAreas();
	std::vector<std::vector<float>> Content;
	void PrintAreas(std::string outputPath);
private:
	std::vector<std::vector<float>> parseArea(std::vector<int> firstAreaIndices, std::vector<int> secondAreaIndices);

	FunctionalNetworkType _networkType;
	CorrelationType _correlationType;
};