f1 = fopen('d:/home/Faculta/research_stuff/Date Dots_30/Dots_30_001/Dots_30_001-Ch001.bin');
f2 = fopen('d:/home/Faculta/research_stuff/Date Dots_30/Dots_30_001/Dots_30_001-Ch002.bin');
f3 = fopen('d:/home/Faculta/research_stuff/Date Dots_30/Dots_30_001/Dots_30_001-Ch005.bin');
f4 = fopen('d:/home/Faculta/research_stuff/Date Dots_30/Dots_30_001/Dots_30_001-Ch020.bin');
f5 = fopen('d:/home/Faculta/research_stuff/Date Dots_30/Dots_30_001/Dots_30_001-Ch050.bin');
f6 = fopen('d:/home/Faculta/research_stuff/Date Dots_30/Dots_30_001/Dots_30_001-Ch100.bin');
F1 = fread(f1, 'float32');
F2 = fread(f2, 'float32');
F3 = fread(f3, 'float32');
F4 = fread(f4, 'float32');
F5 = fread(f5, 'float32');
F6 = fread(f6, 'float32');

F1 = F1(1:1500);
F2 = F2(1:1500);
F3 = F3(1:1500);
F4 = F4(1:1500);
F5 = F5(1:1500);
F6 = F6(1:1500);

vals = zeros(1, 14);

pearson = [corr(F1, F1) corr(F1, F2) corr(F1, F3) corr(F1, F4) corr(F1, F5) corr(F1, F6);
corr(F2, F1) corr(F2, F2) corr(F2, F3) corr(F2, F4) corr(F2, F5) corr(F2, F6);
corr(F3, F1) corr(F3, F2) corr(F3, F3) corr(F3, F4) corr(F3, F5) corr(F3, F6);
corr(F4, F1) corr(F4, F2) corr(F4, F3) corr(F4, F4) corr(F4, F5) corr(F4, F6);
corr(F5, F1) corr(F5, F2) corr(F5, F3) corr(F5, F4) corr(F5, F5) corr(F5, F6);
corr(F6, F1) corr(F6, F2) corr(F6, F3) corr(F6, F4) corr(F6, F5) corr(F6, F6)];

subplot(6, 2, 1);
plot(F1);
title('Canal A');
ylabel('mV^2');
hold on;
subplot(6, 2, 3);
plot(F2);
title('Canal B');
ylabel('mV^2');
hold on;
subplot(6, 2, 5);
plot(F3);
title('Canal C');
ylabel('mV^2');
hold on;
subplot(6, 2, 7);
plot(F4);
title('Canal D');
ylabel('mV^2');
hold on;
subplot(6, 2, 9);
plot(F5);
title('Canal E');
ylabel('mV^2');
hold on;
subplot(6, 2, 11);
plot(F6);
title('Canal F');
ylabel('mV^2');
hold on;
subplot(6, 2, [2,4,6, 8, 10, 12]);
imagesc(pearson);
set(gca, 'XTickLabel', {'A','B','C','D','E', 'F'});
set(gca, 'YTickLabel', {'A','B','C','D','E', 'F'});
hold on;


fclose(f1);
fclose(f2);
fclose(f3);
fclose(f4);
fclose(f5);
fclose(f6);

% f1 = fopen('s1.txt', 'r');
% f2 = fopen('s2.txt', 'r');
% f3 = fopen('7hz_142ms.txt', 'r'); % 3hz
% f4 = fopen('15hz_66ms.txt', 'r'); % 15hz
% f5 = fopen('31hz_32ms.txt', 'r'); % 31hz
% f6 = fopen('40hz_25ms.txt', 'r'); % 80hz
% 
% a = fscanf(f1, '%f');
% b = fscanf(f2, '%f');
% c = fscanf(f3, '%f');
% d = fscanf(f4, '%f');
% e = fscanf(f5, '%f');
% f = fscanf(f6, '%f');
% 
% x = xcorr(a,b);
% 
% subplot(3, 2, 1);
% plot(a);
% ylabel('mV^2')
% title('Semnal A');
% hold on;
% subplot(3, 2, 2);
% plot(b);
% title('Semnal B');
% ylabel('mV^2')
% hold on; 
% subplot(3, 2, 3);
% plot(c);
% title('Theta (7Hz) / 142ms');
% ylabel('mV^2')
% hold on;
% subplot(3, 2, 4);
% plot(d);
% title('Alpha (15Hz) / 66ms');
% ylabel('mV^2')
% hold on;
% subplot(3, 2, 5);
% plot(e);
% title('Beta (31Hz) / 32ms');
% ylabel('mV^2')
% hold on;
% subplot(3, 2, 6);
% plot(f);
% title('Gamma (40Hz) / 25ms');
% ylabel('mV^2')
% hold on;
% 
% 
% fclose(f1);
% fclose(f2);
% fclose(f3);