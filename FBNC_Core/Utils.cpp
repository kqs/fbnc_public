#pragma once

#include "Utils.h"
#include <vector>
#include <fstream>

static const int _occipital[] = { 12,13,14,15,16,20,21,22,23,24,25,26,27,28,29 };
static const int _frontal[] = { 76,77,78,79,80,81,82,83,89,90,91,92 };
static const int _parietal[] = { 0,1,32,33,51,64,65,74,86,87,96,97,110,111 };
static const int _left_temporal[] = { 103,104,105,116,117,118,119,120,121,125,126,127 };
static const int _right_temporal[] = { 41,42,43,45,46,47,55,56,57,58,59,60 };

std::vector<int> OCCIPITAL(_occipital, _occipital + sizeof(_occipital) / sizeof(_occipital[0]));
std::vector<int> FRONTAL(_frontal, _frontal + sizeof(_frontal) / sizeof(_frontal[0]));
std::vector<int> PARIETAL(_parietal, _parietal + sizeof(_parietal) / sizeof(_parietal[0]));
std::vector<int> LEFT_TEMPORAL(_left_temporal, _left_temporal + sizeof(_left_temporal) / sizeof(_left_temporal[0]));
std::vector<int> RIGHT_TEMPORAL(_right_temporal, _right_temporal + sizeof(_right_temporal) / sizeof(_right_temporal[0]));

int GetMS(int samples)
{
	return static_cast<int>(ceil(static_cast<float>(samples) * 1000 / FREQUENCY));
}

int GetSamples(int ms)
{
	return static_cast<int>(ceil(static_cast<float>(ms) * FREQUENCY / 1000));
}

std::string FormatInputEvents(int events[3])
{
	std::string formattedEvents = std::to_string(events[0]);
	if (events[1] != -1 && events[2] == -1)
		formattedEvents += ("_" + std::to_string(events[1]));
	if (events[1] != -1 && events[2] != -1)
		formattedEvents += ("_" + std::to_string(events[1]) + "_" + std::to_string(events[2]));

	return formattedEvents;
}

std::string ToString(CorrelationType ct)
{
	switch (ct)
	{
		case CorrelationType::CROSS_CORRELATION:
			return "CC";
		case CorrelationType::SCALED_CORRELATION:
			return "SC";
		case CorrelationType::PEARSON_CORRELATION:
			return "PC";
		default:
			return "[Unkown correlation]";
	}
}

std::string FormatCurrentExecution(std::string subjectName, CorrelationType corrType, int eventsInterval[3])
{
	return std::string("[" + subjectName + "][" + ToString(corrType) + "][" + FormatInputEvents(eventsInterval) + "]");
}

void WriteMatrixToFile(std::vector<std::vector<float>> matrix, std::string matrixFileNamepath)
{
	std::ofstream f1(matrixFileNamepath);
	for (size_t i = 0; i < matrix.size(); ++i)
	{
		for (size_t j = 0; j < matrix[i].size(); ++j)
		{
			f1 << matrix[i][j] << " ";
		}
		f1 << "\n";
	}
	f1.close();
}

void WriteVectorToFile(std::vector<float> matrix, std::string vectorFileNamepath)
{
	std::ofstream f1(vectorFileNamepath);
	for (size_t i = 0; i < matrix.size(); ++i)
	{
		f1 << matrix[i] << " ";
	}
	f1.close();
}

std::vector<std::vector<float>> ReadMatrixFromFile(std::string fileName)
{
	std::ifstream matrix(fileName);

	std::vector<std::vector<float>> m;
	m.resize(128);
	for (auto i = 0; i < 128; ++i)
		m[i].resize(128);

	for (auto i = 0; i < 128; ++i)
	{
		for (auto j = 0; j < 128; ++j)
		{
			matrix >> m[i][j];
		}
	}

	matrix.close();
	return m;
}

void MarshalString(System::String^ s, std::string& os) {
	const char* chars =
		(const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
	os = chars;
	System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void*)chars));
}