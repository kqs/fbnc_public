#include <iostream>
#include <vector>
#include <algorithm>
#include "FunctionalNetworkCreator.h"
#include "libs\correlation_lib\CrossCorrelation-CC.h"
#include "libs\correlation_lib\ScaledCorrelation-CC.h"
#define M_PI 3.14159265358979323846

using namespace System;

//void computeIQR(std::vector<float> vec, float *q1, float *q2, float *q3, float *iqr)
//{
//	auto vecSize = vec.size();
//	auto halfVecsize = vecSize / 2;
//
//	std::cout << "vec size: " << vecSize << std::endl;
//	std::cout << "half vec size : " << halfVecsize << std::endl;
//
//	if (vecSize % 2 == 0)
//	{
//		std::cout << "even nbs..." << std::endl;
//		*q2 = (float)(vec.at(halfVecsize - 1) + vec.at(halfVecsize)) / 2;
//	}
//	else
//	{
//		std::cout << "odd nbs..." << std::endl;
//		*q2 = vec.at(halfVecsize);	
//	}
//
//	if (halfVecsize % 2 == 0)
//	{
//		*q1 = (vec.at(vecSize * 1 / 4) + vec.at(vecSize * 1 / 4 - 1)) / 2;
//		*q3 = (vec.at(vecSize * 3 / 4) + vec.at(vecSize * 3 / 4 - 1)) / 2;
//
//		*iqr = *q3 - *q1;
//	}
//	else if (halfVecsize % 2 != 0)
//	{
//		*q1 = vec.at(vecSize * 1 / 4);
//		*q3 = vec.at(vecSize * 3 / 4);
//		*iqr = *q3 - *q1;
//	}
//}

int main(array<System::String ^> ^args)
{
    /*Console::WriteLine(L"Hello World");

	std::vector<float> vec = std::vector<float>();
	vec.push_back(53);
	vec.push_back(39);
	vec.push_back(39);
	vec.push_back(33);
	vec.push_back(69);
	vec.push_back(30);
	vec.push_back(25);
	vec.push_back(67);
	vec.push_back(130);
	vec.push_back(94);
	vec.push_back(40);

	std::sort(vec.begin(), vec.end());

	for (int i = 0; i < vec.size(); ++i)
		std::cout << vec[i] << " ";
	std::cout << std::endl;

	float q1, q3, q2, iqr;
	computeIQR(vec, &q1, &q2, &q3, &iqr);

	std::cout << "Q1 =  " << q1 << std::endl;
	std::cout << "Q2 =  " << q2 << std::endl;
	std::cout << "Q3 =  " << q3 << std::endl;
	std::cout << "IQR = " << iqr << std::endl;*/

	//std::string t1("D:/Data/Dots_30_001-Ch%03d.bin");
	//std::string t2("D:/Data/Dots_30_001-Event-Codes.bin");
	//std::string t3("D:/Data/Dots_30_001-Event-Timestamps.bin");
	//std::string t4("D:/Data/Results");

	//int a[3] = { 129, 1, -1 };
	////FunctionalNetworkCreator networkCreator(t1, t2, t3, t4, "s1", &callback);
	////networkCreator.Generate(CorrelationType::CROSS_CORRELATION, a);

	////std::vector<float> val1 = EEGFileProcessor::ReadFile<float>("d:/home/Faculta/research_stuff/Date Dots_30/Dots_30_002/Dots_30_002-Ch001.bin");
	////std::vector<float> val2 = EEGFileProcessor::ReadFile<float>("d:/home/Faculta/research_stuff/Date Dots_30/Dots_30_002/Dots_30_002-Ch088.bin");
	///*std::vector<float> val1 = std::vector<float>(); val1.push_back(1); val1.push_back(6); val1.push_back(2);  val1.push_back(4); val1.push_back(2);
	//std::vector<float> val2 = std::vector<float>(); val2.push_back(4); val2.push_back(2); val2.push_back(1);  val2.push_back(7); val2.push_back(5);

	//auto someVals1 = std::vector<float>(val1.begin() + 0, val1.begin() + 5);
	//auto someVals2 = std::vector<float>(val2.begin() + 0, val2.begin() + 5);*/

	//std::vector<float> _arr1;
	//std::vector<float> _arr2;
	//int limit = 100;
	//for (auto i = 0; i < limit; ++i) {
	//	float sample1 = 1*sin(2.0 * M_PI / 1000 * 100 * i) + 0.5*sin(2.0 * M_PI / 1000 * 10 * i);
	//	float sample2 = 1*sin(2.0 * M_PI / 1000 * 100 * i) + 0.5*sin(2.0 * M_PI / 1000 * 10 * i);
	//	_arr1.push_back(sample1);
	//	_arr2.push_back(sample2);
	//}

	//int iError = 0;
	//int winSize = 100;
	//CCrossCorrelationCC cc(winSize, 1000, iError);
	//CScaledCorrelationCC sc(100, winSize, 1000, iError);
	//cc.ComputeCrossCorrelation(&_arr1[0], _arr1.size(), &_arr2[0], _arr2.size(), 0);
	//sc.ComputeScaledCorrelationNoBorderChecking(&_arr1[0], _arr1.size(), &_arr2[0], _arr2.size(), 0);
	//float *correlogram = cc.GetCrossCorrelogram();
	//float *correlogram1 = sc.GetScaledCrossCorrelogram();
	//std::vector<float> corr;
	//std::vector<float> scorr;

	//for (auto k = 0; k < 2 * winSize + 1; ++k)
	//{
	//	if (correlogram[k] != NAN) {
	//		corr.push_back(correlogram[k]);
	//	}
	//	if (correlogram1[k] != NAN)
	//	{
	//		if (correlogram1[k] > 1 || correlogram1[k] < -1)
	//			scorr.push_back(0);
	//		else scorr.push_back(correlogram1[k]);
	//	}
	//	else
	//	{
	//		std::cout << "aoleu";
	//	}
	//}

	//std::ofstream f1("s1.txt");
	//std::ofstream f2("s2.txt");
	//std::ofstream f3("cross.txt");
	//std::ofstream f4("scaled.txt");
	//for (size_t i = 0; i < _arr1.size(); ++i)
	//{
	//	f1 << _arr1[i] << " ";
	//	f2 << _arr2[i] << " ";
	//}
	//for(size_t i = 0; i < corr.size(); ++i)
	//{
	//	f3 << corr[i] << " ";
	//}
	//for (size_t i = 0; i < scorr.size(); ++i)
	//{
	//	f4 << scorr[i] << " ";
	//}
	//f1.close();
	//f2.close();
	//f3.close();
	//f4.close();
    return 0;
}
