#pragma once

#include <iostream>
#include <vector>

using namespace System;
using namespace System::Collections::Generic;

public ref class MetricsAnalyzer
{

private:
	std::vector<float> bellmanFord(const std::vector<std::vector<float>> network, int source);
	float computeAveragePathLength(std::vector<std::vector<float>> network);
public:
	MetricsAnalyzer();
	array<float>^ GetAveragePathLength(array<System::String^> ^inputFilePaths);
};