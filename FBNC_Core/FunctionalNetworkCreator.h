#pragma once

#include "FunctionalNetwork.h"
#include "EEGFileProcessor.h"

#include <iostream>
#include <string>
#include <vector>

class FunctionalNetworkCreator
{
public:
	FunctionalNetworkCreator(
		std::string binDataPath,
		std::string eventCodesPath,
		std::string eventTimestampsPath,
		std::string outputPath,
		std::string subjectName,
		void(*progressCallback)(std::string));
	void Generate(CorrelationType corrType, int eventsInterval[3]);
	
private:
	std::vector<FunctionalNetwork> generateNetwork(std::vector<std::vector<float>> &channelsEventBinaryData, CorrelationType corrType);
	std::vector<std::vector<float>> getMeanMatrix(std::vector<FunctionalNetwork> matrices);
	void getLagAndPeak(std::vector<float> &correlation, long *delay, float *peak);
	EEGFileProcessor *fileProcessor;

	std::string _outputPath;
	std::string _subjectName;	
	void(*_progressCallback)(std::string);
};
