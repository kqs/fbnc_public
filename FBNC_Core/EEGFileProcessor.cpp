#include "EEGFileProcessor.h"
#include <omp.h>
#include <time.h>

EEGFileProcessor::EEGFileProcessor(
	std::string binDataPath, std::string eventCodesPath, std::string eventTimestampsPath, void (*progressCallback)(std::string))
: _progressCallback(progressCallback)
{
	if (binDataPath.empty() || eventCodesPath.empty() || eventTimestampsPath.empty()) {
		throw gcnew System::Exception("Error: One of given paths are empty!");
	}

	_binDataPath = std::string(binDataPath);
	_eventCodes = ReadFile<int32_t>(eventCodesPath);
	_eventTimestamps = ReadFile<int32_t>(eventTimestampsPath);

	std::cout << "Gathering channels binary data..." << std::endl;
	_progressCallback("Gathering channels binary data...");
	time_t t0 = time(NULL);
	_channelsBinaryData = GetChannelsBinaryData<float>();
	time_t t1 = time(NULL);
	std::cout << "Gathering channels binary data....DONE [" << std::to_string(t1 - t0) << "sec]" << std::endl;
	_progressCallback(std::string("Gathering channels binary data....DONE [" + std::to_string(t1 - t0) + "sec]"));
}

std::vector<std::pair<int, int>> EEGFileProcessor::getTrialsPositionByEvent(int firstEvent, int secondEvent = -1, int thirdEvent = -1)
{
	std::vector<std::pair<int, int>> trials_map;

	for (size_t i = 0; i < _eventCodes.size(); ++i)
	{
		if (_eventCodes[i] == firstEvent)
		{
			if (secondEvent != -1 && _eventCodes[i + 1] != secondEvent)
				continue;
			if (thirdEvent != -1 && _eventCodes[i + 2] != thirdEvent)
				continue;

			trials_map.push_back(std::make_pair(_eventTimestamps[i], _eventTimestamps[i + 1]));
		}
	}
	return trials_map;
}