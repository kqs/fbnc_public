#pragma once

#include "Utils.h"
#include <iostream>
#include <fstream>
#include <istream>
#include <iterator>
#include <string>
#include <map>
#include <vector>
#include <omp.h>

class EEGFileProcessor
{
public:
	EEGFileProcessor(std::string binDataPath, std::string eventCodesPath, std::string eventTimestampsPath, void(*progressCallback)(std::string));

	template <class T> std::vector<std::vector<T>> GetChannelsBinaryData()
	{
		std::vector<std::vector<T>> channelsBinaryData;
		channelsBinaryData.resize(CHANNELS_NB);
		
		#pragma omp parallel for
		for (auto i = 1; i <= CHANNELS_NB; ++i)
		{
			char buff[255];
			snprintf(buff, sizeof(buff), _binDataPath.c_str(), i);

			channelsBinaryData[i-1] = ReadFile<float>(std::string(buff));
		}

		return channelsBinaryData;
	}

	template <class T> std::vector<std::vector<std::vector<T>>> GetEventsBinaryData(int firstEvent, int secondEvent, int thirdEvent)
	{
		// Dimension 1: each event E
		// Dimension 2: each channel C for event E
		// Dimension 3: values of event E for channel C
		std::vector<std::vector<std::vector<T>>> eventsBinaryData;
		std::vector<std::pair<int, int>> eventsPairs = getTrialsPositionByEvent(firstEvent, secondEvent, thirdEvent);
		for (size_t i = 0; i < eventsPairs.size(); ++i)
		{
			std::vector<std::vector<float>> eventChannels;
			eventChannels.resize(CHANNELS_NB);

			for (auto j = 0; j < CHANNELS_NB; ++j)
			{
				auto channel = _channelsBinaryData[j];
				eventChannels[j] = GetSamplesByRange<float>(channel, eventsPairs[i].first, eventsPairs[i].second);
			}
			eventsBinaryData.push_back(eventChannels);
		}

		return eventsBinaryData;
	}

	template <class T> std::vector<T> static ReadFile(std::string filePath)
	{
		std::ifstream fileStream(filePath, std::ios::binary | std::ios::in);

		if (!fileStream.good()) {
			/*throw System::InvalidOperationException("Cannot open file " + filePath);*/
			System::String^ str = System::String::Concat("Cannot open file ", gcnew System::String(filePath.c_str()));
			throw gcnew System::Exception(str);
		}

		std::vector<T> values;
		fileStream.seekg(0, std::ios::end);
		long filesize = fileStream.tellg() / sizeof(float);
		fileStream.seekg(0, std::ios::beg);

		values.resize(filesize);

		int i = 0;
		while (true) {
			T n;
			fileStream.read(reinterpret_cast<char*>(&n), sizeof n);
			if (fileStream.eof()) break;

			values[i] = n;
			i++;
		}

		fileStream.close();
		return values;
	}

private:
	
	std::vector<std::pair<int, int>> getTrialsPositionByEvent(int firstEvent, int secondEvent, int thirdEvent);

	std::vector<std::vector<float>> _channelsBinaryData;
	std::string _binDataPath;
	std::vector<int32_t> _eventCodes;
	std::vector<int32_t> _eventTimestamps;
	void(*_progressCallback)(std::string);
};