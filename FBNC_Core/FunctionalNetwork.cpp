#include "FunctionalNetwork.h"

FunctionalNetwork::FunctionalNetwork()
{
}

FunctionalNetwork::FunctionalNetwork(std::vector<std::vector<float>> dataContent, FunctionalNetworkType networkType, CorrelationType corrType)
{
	Content = dataContent;
	_networkType = networkType;
	_correlationType = corrType;
}

std::vector<std::vector<float>> FunctionalNetwork::parseArea(std::vector<int> firstAreaIndices, std::vector<int> secondAreaIndices)
{
	std::vector<std::vector<float>> areaMatrix;
	areaMatrix.resize(firstAreaIndices.size());
	for (auto i = 0; i < firstAreaIndices.size(); ++i)
		areaMatrix[i].resize(secondAreaIndices.size());

	for (auto i = 0; i < firstAreaIndices.size(); ++i)
	{
		for (auto j = 0; j < secondAreaIndices.size(); ++j)
		{
			areaMatrix[i][j] = Content[firstAreaIndices[i]][secondAreaIndices[j]];
		}
	}

	return areaMatrix;
}

BrainAreas FunctionalNetwork::ParseAreas()
{
	BrainAreas brainAreas;
	brainAreas.occipital_frontal = parseArea(OCCIPITAL, FRONTAL);
	brainAreas.occipital_parietal = parseArea(OCCIPITAL, PARIETAL);
	brainAreas.occipital_leftTemporal = parseArea(OCCIPITAL, LEFT_TEMPORAL);
	brainAreas.occipital_rightTemporal = parseArea(OCCIPITAL, RIGHT_TEMPORAL);
	brainAreas.frontal_parietal = parseArea(FRONTAL, PARIETAL);
	brainAreas.frontal_leftTemporal = parseArea(FRONTAL, LEFT_TEMPORAL);
	brainAreas.frontal_rightTemporal = parseArea(FRONTAL, RIGHT_TEMPORAL);
	brainAreas.parietal_leftTemporal = parseArea(PARIETAL, LEFT_TEMPORAL);
	brainAreas.parietal_rightTemporal = parseArea(PARIETAL, RIGHT_TEMPORAL);
	brainAreas.leftTemporal_rightTemporal = parseArea(LEFT_TEMPORAL, RIGHT_TEMPORAL);
	return brainAreas;
}

void FunctionalNetwork::PrintAreas(std::string outputPath)
{
	BrainAreas areas = ParseAreas();

	WriteMatrixToFile(areas.occipital_frontal, std::string(outputPath + "/occipital_frontal.txt"));
	WriteMatrixToFile(areas.occipital_parietal, std::string(outputPath + "/occipital_parietal.txt"));
	WriteMatrixToFile(areas.occipital_leftTemporal, std::string(outputPath + "/occipital_leftTemporal.txt"));
	WriteMatrixToFile(areas.occipital_rightTemporal, std::string(outputPath + "/occipital_rightTemporal.txt"));

	WriteMatrixToFile(areas.frontal_parietal, std::string(outputPath + "/frontal_parietal.txt"));
	WriteMatrixToFile(areas.frontal_leftTemporal, std::string(outputPath + "/frontal_leftTemporal.txt"));
	WriteMatrixToFile(areas.frontal_rightTemporal, std::string(outputPath + "/frontal_rightTemporal.txt"));

	WriteMatrixToFile(areas.parietal_leftTemporal, std::string(outputPath + "/parietal_leftTemporal.txt"));
	WriteMatrixToFile(areas.parietal_rightTemporal, std::string(outputPath + "/parietal_rightTemporal.txt"));

	WriteMatrixToFile(areas.leftTemporal_rightTemporal, std::string(outputPath + "/leftTemporal_rightTemporal.txt"));
}