#include "PACO.h"
#include <list>
#include <algorithm>
#include <iterator>

PACO::PACO()
{
}

std::vector<int> PACO::findNeighbors(std::vector<std::vector<float>> matrix, int node)
{
	std::vector<int> neighbors;
	for (size_t i = 0; i < matrix.size(); ++i)
	{
		if (matrix[node][i] != 0 || matrix[i][node] != 0)
			neighbors.push_back(i);
	}

	return neighbors;
}

std::vector<edge> PACO::getEdgesSortedByJaccard(std::vector<std::vector<float>> matrix)
{
	std::vector<edge> sortedEdges;

	for (size_t i = 0; i < matrix.size(); ++i)
	{
		for (size_t j = i + 1; j < matrix[i].size(); ++j)
		{
			// found an edge
			if (matrix[i][j] != 0)
			{
				auto neighsU = findNeighbors(matrix, i);
				auto neighsV = findNeighbors(matrix, j);

				std::sort(neighsU.begin(), neighsU.end());
				std::sort(neighsV.begin(), neighsV.end());

				std::vector<int> setIntersection, setUnion;
				std::set_intersection(neighsU.begin(), neighsU.end(), neighsV.begin(), neighsV.end(), std::back_inserter(setIntersection));
				std::set_union(neighsU.begin(), neighsU.end(), neighsV.begin(), neighsV.end(), std::back_inserter(setUnion));

				float jaccardIndex = static_cast<float>(setIntersection.size()) / setUnion.size();

				edge* _edge = new edge(i, j, matrix[i][j], jaccardIndex);
				sortedEdges.push_back(*_edge);
			}
		}
	}

	std::sort(sortedEdges.begin(), sortedEdges.end());

	return sortedEdges;
}

int PACO::getCommunity(std::vector<std::vector<int>> communities, int node)
{
	for (size_t i = 0; i < communities.size(); ++i)
	{
		if (std::find(communities[i].begin(), communities[i].end(), node) != communities[i].end())
			return i;
	}
	return -1;
}

void PACO::migrateToCommunity(std::vector<std::vector<int>> &communities, int node, int newCommunityIndex, int oldCommunityIndex)
{
	// move to new community
	communities[newCommunityIndex].push_back(node);

	// remove from old community
	communities[oldCommunityIndex].erase(
		std::remove(communities[oldCommunityIndex].begin(), communities[oldCommunityIndex].end(), node),
		communities[oldCommunityIndex].end()
	);
}

double PACO::getNumberOfEdges(std::vector<std::vector<float>> matrix)
{
	double edgesNb = 0;
	for (size_t i = 0; i < matrix.size(); ++i)
	{
		for (size_t j = i + 1; j < matrix[i].size(); ++j)
		{
			if (matrix[i][j] != 0)
				edgesNb += matrix[i][j];
		}
	}
	return edgesNb;
}

std::vector<std::vector<std::vector<float>>> PACO::getCommunitiesGraphs(std::vector<std::vector<float>> graph, std::vector<std::vector<int>> communities)
{
	std::vector<std::vector<std::vector<float>>> communitiesGraphs;

	// iterate through all communities
	for (size_t i = 0; i < communities.size(); ++i)
	{
		int communitySize = communities[i].size();
		if (communitySize == 0) continue;

		std::vector<std::vector<float>> communityGraph;
		communityGraph.resize(communitySize);
		for (size_t j = 0; j < communitySize; ++j)
			communityGraph[j].resize(communitySize);

		// extract from parent graph the subgraphs according to each community
		for (size_t x1 = 0; x1 < communitySize; ++x1)
		{
			for (size_t x2 = x1 + 1; x2 < communitySize; ++x2)
			{
				int firstEdge = communities[i][x1];
				int secondEdge = communities[i][x2];
				if (graph[firstEdge][secondEdge] != 0)
					communityGraph[x1][x2] = graph[firstEdge][secondEdge];
				if (graph[secondEdge][firstEdge] != 0)
					communityGraph[x2][x1] = graph[secondEdge][firstEdge];
			}
		}

		communitiesGraphs.push_back(communityGraph);
	}

	return communitiesGraphs;
}


// Taken from: https://github.com/CarloNicolini/paco/blob/8079b89a9263c619d7f5ff6e5d5ba5665246f75d/src/paco/KLDivergence.cpp
double PACO::KL(double q, double p)
{
	double KL = 0.0;
	if (q > 0.0 && p > 0.0)
		KL += q*log(q / p);
	if (q < 1.0 && p < 1.0)
		KL += (1.0 - q)*log((1.0 - q) / (1.0 - p));
	return KL;
}

double PACO::ASYMPTOTIC_SURPRISE(std::vector<std::vector<float>> matrix, std::vector<std::vector<int>> communities)
{
	double totalWeightesIntraCluster = 0.0;
	double totalPairsIntraCluster = 0.0;
	double totalWeights = getNumberOfEdges(matrix);
	double totalPairs = 8128.0; // for 128 nodes

	auto communitiesGraphs = getCommunitiesGraphs(matrix, communities);
	for (size_t i = 0; i < communitiesGraphs.size(); ++i)
	{
		auto communityGraph = communitiesGraphs[i];
		totalWeightesIntraCluster += getNumberOfEdges(communityGraph);
		totalPairsIntraCluster += (communityGraph.size() * (communityGraph.size() - 1)) / 2;
	}

	double m = totalWeights;
	double mi = totalWeightesIntraCluster;

	double p = totalPairs;
	double pi = totalPairsIntraCluster;

	double quality = m*KL(mi / m, pi / p);
	return quality;
}

std::vector<std::vector<int>> PACO::computePACO(std::vector<std::vector<float>> matrix)
{
	float surprise = 0.0f;
	// initially have 128 communities (128 vertices)
	std::vector<std::vector<int>> communities;
	communities.resize(matrix.size());
	for (size_t i = 0; i < matrix.size(); ++i)
		communities[i].push_back(i);

	std::vector<edge> sortedEdges = getEdgesSortedByJaccard(matrix);

	std::cout << "Sorted edges: " << sortedEdges.size() << std::endl;
	for (size_t i = 0; i < sortedEdges.size(); ++i)
	{
		std::cout << "Processing edge nb. " << i << std::endl;
		edge _edge = sortedEdges[i];

		int communityIndexNode1 = getCommunity(communities, _edge._from);
		int communityIndexNode2 = getCommunity(communities, _edge._to);
		if (communityIndexNode1 != communityIndexNode2)
		{
			std::vector<std::vector<int>> tempCommunities = communities;
			float randomValue = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			if (randomValue <= 0.50f)
				migrateToCommunity(tempCommunities, _edge._from, communityIndexNode2, communityIndexNode1);
			else
				migrateToCommunity(tempCommunities, _edge._to, communityIndexNode1, communityIndexNode2);

			/*std::cout << "We have " << communities.size() << " communities" << std::endl;
			for (auto i = 0; i < communities.size(); ++i)
			{
			std::cout << communities[i].size() << " ";
			}*/

			float tempSurprise = ASYMPTOTIC_SURPRISE(matrix, tempCommunities);

			/*std::cout << "TEMP SURPRISE: " << tempSurprise << " | ACTUAL SURPRISE: " << surprise << std::endl;*/
			if (tempSurprise > surprise)
			{
				communities = tempCommunities;
				surprise = tempSurprise;
			}
		}
	}

	return communities;
}