#include "FunctionalNetworkCreatorBridge.h"
#include "FunctionalNetworkCreator.h"

#include <string>  
#include <iostream>
#include <windows.h> 
using namespace std;
using namespace System;

FunctionalNetworkCreatorBridge::FunctionalNetworkCreatorBridge(
	System::String^ binDataPath,
	System::String^ eventCodesPath,
	System::String^ eventTimestampsPath,
	System::String^ outputPath,
	System::String^ subjectName, 
	ProgressCallback^ progressCallback)
{
	string binDataPathStr, eventCodesPathStr, eventTimestampsPathStr, outputPathStr, subjectNameStr;
	MarshalString(binDataPath, binDataPathStr);
	MarshalString(eventCodesPath, eventCodesPathStr);
	MarshalString(eventTimestampsPath, eventTimestampsPathStr);
	MarshalString(outputPath, outputPathStr);
	MarshalString(subjectName, subjectNameStr);

	_progressDelegate = progressCallback;
	IntPtr pointer = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(_progressDelegate);
	_progressCallback = static_cast<void(*)(std::string)>(pointer.ToPointer());
	_creator = new FunctionalNetworkCreator(binDataPathStr, eventCodesPathStr, eventTimestampsPathStr, outputPathStr, subjectNameStr, _progressCallback);
}

void FunctionalNetworkCreatorBridge::Generate(CorrelationTypeBridge corrType, int leftBound, int middleBound, int rightBound)
{
	int eventsFiltering[3];
	eventsFiltering[0] = leftBound;
	eventsFiltering[1] = middleBound;
	eventsFiltering[2] = rightBound;

	FunctionalNetworkCreator* creator = static_cast<FunctionalNetworkCreator*>(_creator);
	creator->Generate((CorrelationType)corrType, eventsFiltering);
}