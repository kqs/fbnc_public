**** THIS FILE PROVIDES INFORMATION ABOUT EXISTING FILES IN CURRENT DIRECTORY ****
----------------------------------------------------------------------------------
* FBNC_Core - C++ Solution
* FBNC_Core - Desktop App (WPF C# App)
* FunctionalViewer - Unity Project
* Mathematica - scripts used initially to generate some metrics (not used anymore)
* Matlab - scripts used to generate plots
* PACO - algorithm compiled from https://github.com/CarloNicolini/paco
* R - R scripts to compute some metrics
* Results - some diagrams of results
* FBNC.sln - solution of main project

* Other folders - Utility folders needed by other projects (unity, fbnc, etc.)