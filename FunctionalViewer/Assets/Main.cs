﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using Assets;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.UI;
using Random = System.Random;

public static class RandomExtensions
{
    public static double NextDouble(this Random RandGenerator, double MinValue, double MaxValue)
    {
        return RandGenerator.NextDouble() * (MaxValue - MinValue) + MinValue;
    }
}

public class NetworkPair
{
    public int SourceNode { get; set; }
    public int DestNode { get; set; }
    public float Weight { get; set; }

    public NetworkPair(int source, int dest, float weight)
    {
        SourceNode = source;
        DestNode = dest;
        Weight = weight;
    }
}

public class Main : MonoBehaviour
{
    readonly int[] _occipital = { 12, 13, 14, 15, 16, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 };
    readonly int[] _frontal = { 76, 77, 78, 79, 80, 81, 82, 83, 89, 90, 91, 92 };
    readonly int[] _parietal = { 0, 1, 32, 33, 51, 64, 65, 74, 86, 87, 96, 97, 110, 111 };
    readonly int[] _leftTemporal = { 103, 104, 105, 116, 117, 118, 119, 120, 121, 125, 126, 127 };
    readonly int[] _rightTemporal = { 41, 42, 43, 45, 46, 47, 55, 56, 57, 58, 59, 60 };

    private readonly Dictionary<int, Vector3> _capCoordinates = new Dictionary<int, Vector3>();
    private readonly Dictionary<int, string> _capLabels = new Dictionary<int, string>();

    private readonly List<GameObject> _nodes = new List<GameObject>();
    private readonly List<LineRenderer> _edges = new List<LineRenderer>();
    private readonly List<GameObject> _arrows = new List<GameObject>();

    private readonly List<Color> _lineColors = new List<Color>();

    private readonly List<int> _sourceNodes = new List<int>();
    private readonly List<int> _destNodes = new List<int>();
    private readonly List<float> _weights = new List<float>();

    private readonly List<NetworkPair> _pairs = new List<NetworkPair>();

    public TextAsset HeadcapCoordinates;
    public TextAsset SourceOutput;
    public TextAsset DestinationOutput;
    public TextAsset WeightOutput;
    public TextAsset MatrixSource;

    private bool _showLabels = true;

    private string _rawData;
    private string _networkData = null;

    private Text _text;

    private Vector3 mousePos;
    public GameObject target;
    private Vector3 objectPos;
    private float angle;
    private float _percentToDisplay = 1.0f;

    private float _scaleIndex = 1.0f;

    private CameraOrbit _camera;
    private float MAX;
    private float MIN;

    // <unity_player> <network type> <file>
    private string[] _arguments;

    // 0 - Functional Network
    // 1 - Community
    private int _networkType;

    public Main()
    {
        _arguments = Environment.GetCommandLineArgs();
    }

    public static float ConvertRange(
        float originalStart, float originalEnd, // original range
        float newStart, float newEnd, // desired range
        float value) // value to convert
    {
        double scale = (double)(newEnd - newStart) / (originalEnd - originalStart);
        return (float)(newStart + ((value - originalStart) * scale));
    }

    private void SetExternalSource(string[] arguments)
    {
        if (arguments.Length == 1) return;

        if (!int.TryParse(arguments[1], out _networkType))
        {
            DebugConsole.Log("Second argument is invalid. It has to be either 0 or 1", "error");
            return;
        }

        try
        {
            _rawData = File.ReadAllText(_arguments[2]);
            if (_networkType == 1)
            {
                _networkData = File.ReadAllText(_arguments[3]);
            }
        }
        catch (Exception ex)
        {
            DebugConsole.Log(ex.Message, "error");
        }
    }

    void Start()
    {
        DebugConsole.Log("------------------------");
        DebugConsole.Log("Object rotations:");
        DebugConsole.Log("------------------------");
        DebugConsole.Log("← : left");
        DebugConsole.Log("→ : right");
        DebugConsole.Log("↑ : upwards");
        DebugConsole.Log("↓ : downwards");
        DebugConsole.Log("------------------------");
        DebugConsole.Log("Camera actions:");
        DebugConsole.Log("------------------------");
        DebugConsole.Log("Left Click : move around object");
        DebugConsole.Log("Scroll : zoom in/out");

        DebugConsole.Log("------------------------");
        DebugConsole.Log("Hotkeys:");
        DebugConsole.Log("------------------------");
        DebugConsole.Log("H : hide labels (toggle)");
        DebugConsole.Log("+ : increase density (by 0.5%)");
        DebugConsole.Log("- : decrease density (by 0.5%)");

        _camera = GetComponent<CameraOrbit>();
        _text = GameObject.Find("densityStatus").GetComponent<Text>();

        // @"D:\work\licenta\paco\build\membership.txt"
        // @"d:/Data/Results_last/network_lags/s9/CC/129_1/network_lags_s9_CC_129_1_trial_50.txt"
        //_arguments = new string[] { "", "1", @"d:\home\Faculta\fbnc\PACO\membership.txt", @"d:\home\Faculta\fbnc\PACO\membership_matrix.txt" };

        SetExternalSource(_arguments);
        ParseCoordinates();

        switch (_networkType)
        {
            case 0: // functional network
                if (MatrixSource != null)
                    ParseMatrixData(MatrixSource);
                else if (SourceOutput != null && DestinationOutput != null && WeightOutput != null)
                {
                    ParseData(SourceOutput, _sourceNodes);
                    ParseData(DestinationOutput, _destNodes);
                    ParseData(WeightOutput, _weights);
                }
                else
                {
                    ParseMatrixData(_rawData);
                }

                SetNodesColor(null);
                break;
            case 1: // communities
                var communities = _rawData.Trim().Split('\n').Select(int.Parse).ToList();

                SetNodesColor(communities);
                ParseMatrixData(_networkData);

                _scaleIndex = 2.5f;
                break;
        }

        DrawNodes();
        SetMaxAndMin();
        DrawConnections();

        //Cursor.lockState = CursorLockMode.Locked;
    }

    private void SetMaxAndMin()
    {
        if (_weights.Count == 0)
        {
            MAX = _pairs.Select(p => p.Weight).Max();
            MIN = _pairs.Select(p => p.Weight).Min();
        }
        else
        {
            MAX = _weights.Max();
            MIN = _weights.Min();
        }
    }

    private void DrawConnections()
    {
        if (_sourceNodes.Count() != 0)
        {
            for (var i = 0; i < _sourceNodes.Count; ++i)
            {
                DrawConnection(_sourceNodes[i], _destNodes[i], _weights[i]);
            }
        }
        else
        {
            for (var i = 0; i < _pairs.Count * _percentToDisplay; ++i)
            {
                var element = _pairs.ElementAt(i);
                DrawConnection(element.SourceNode, element.DestNode, element.Weight);
            }
        }
    }

    private void SetNodesColor(List<int> communities)
    {
        // RANDOM COLORS
        //var rndR = (float)new Random(Guid.NewGuid().GetHashCode()).NextDouble();
        //var rndG = (float)new Random(Guid.NewGuid().GetHashCode()).NextDouble();
        //var rndB = (float)new Random(Guid.NewGuid().GetHashCode()).NextDouble();
        //_lineColors.Add(new Color(rndR, rndG, rndB));
        if (communities == null)
        {
            for (var i = 0; i < _capCoordinates.Count; ++i)
            {
                if (_occipital.Contains(i))
                    _lineColors.Add(new Color(1.0f, 0.0f, 0.0f));
                else if (_frontal.Contains(i))
                    _lineColors.Add(new Color(0.0f, 0.0f, 1.0f));
                else if (_parietal.Contains(i))
                    _lineColors.Add(new Color(0.0f, 1.0f, 0.0f));
                else if (_leftTemporal.Contains(i))
                    _lineColors.Add(new Color(1.0f, 1.0f, 0.0f));
                else if (_rightTemporal.Contains(i))
                    _lineColors.Add(new Color(1.0f, 0.0f, 1.0f));
                else
                    _lineColors.Add(new Color(0.0f, 0.0f, 0.0f));
            }
        }
        else
        {
            var nbCommunities = communities.Max();
            var colors = new Color[nbCommunities + 1];
            for (var i = 0; i <= nbCommunities; ++i)
            {
                var red = (float)new Random(Guid.NewGuid().GetHashCode()).NextDouble();
                var green = (float)new Random(Guid.NewGuid().GetHashCode()).NextDouble();
                var blue = (float)new Random(Guid.NewGuid().GetHashCode()).NextDouble();
                colors[i] = new Color(red, green, blue);
            }

            for (var i = 0; i < _capCoordinates.Count; ++i)
            {
                var communityIndex = communities[i];
                _lineColors.Add(colors[communityIndex]);
            }
        }
    }

    private void ParseMatrixData(string content)
    {
        try
        {
            var fileLines = content.Split('\n').Where(l => !string.IsNullOrEmpty(l));
            for (int i = 0; i < fileLines.Count(); ++i)
            {
                var columns = fileLines.ElementAt(i).Split(' ');

                for (int j = 0; j < columns.Length; ++j)
                {
                    float val;
                    float.TryParse(columns[j], out val);
                    if (val == 0 || val == 0.0f) continue;

                    //_sourceNodes.Add(i);
                    //_destNodes.Add(j);
                    //_weights.Add(val);

                    _pairs.Add(new NetworkPair(i, j, val));
                }
            }

            _pairs.Sort((pair1, pair2) => pair2.Weight.CompareTo(pair1.Weight));
        }
        catch (Exception ex)
        {
            DebugConsole.Log(ex.Message, "error");
        }
    }

    private void ParseMatrixData(TextAsset matrixSource)
    {
        ParseMatrixData(matrixSource.text);
    }

    private void ParseData<T>(TextAsset textAsset, List<T> dest)
    {
        foreach (var line in textAsset.text.Split('\n').Where(l => !string.IsNullOrEmpty(l)))
        {
            dest.Add((T)Convert.ChangeType(line, typeof(T)));
        }
    }

    private void DrawConnection(int source, int dest, float weight)
    {
        if (_lineColors[source] == Color.black)
            return;
        Vector3 src = new Vector3(), dst = new Vector3();

        src = _capCoordinates[source];
        dst = _capCoordinates[dest];

        var direction = (dst - src).normalized;

        LineRenderer lineRenderer = new GameObject().AddComponent<LineRenderer>();
        lineRenderer.GetComponent<Renderer>().material = new Material(Shader.Find("Unlit/Color"));
        lineRenderer.GetComponent<Renderer>().material.color = _lineColors[source];
        lineRenderer.GetComponent<Renderer>().material.shader = Shader.Find("Diffuse");
        lineRenderer.startColor = _lineColors[source];
        lineRenderer.endColor = _lineColors[source];
        lineRenderer.SetPosition(0, src);
        lineRenderer.SetPosition(1, dst - direction * 3.0f);
        lineRenderer.widthMultiplier = ConvertRange(MIN, MAX, 0.05f, 0.5f, weight);

        _edges.Add(lineRenderer);

        var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.GetComponent<Renderer>().material = new Material(Shader.Find("Unlit/Color"));
        cube.GetComponent<Renderer>().material.color = _lineColors[source];
        cube.GetComponent<Renderer>().material.shader = Shader.Find("Diffuse");

        cube.transform.position = _capCoordinates[dest] - direction * 3.0f;
        cube.transform.localScale = new Vector3(0.6f, 0.6f, 2.0f);
        cube.transform.rotation = Quaternion.LookRotation(dst - src);
        
        _arrows.Add(cube);
    }

    private void ParseCoordinates()
    {
        int index = 0;
        foreach (var line in HeadcapCoordinates.text.Split('\n').Where(l => !string.IsNullOrEmpty(l)))
        {
            var coordinates = line.Split(',');
            _capCoordinates.Add(index,
                new Vector3(float.Parse(coordinates[1]), float.Parse(coordinates[3]), float.Parse(coordinates[2])));
            _capLabels.Add(index, coordinates[0]);
            index++;
        }
    }

    void DrawNodes()
    {
        for (var i = 0; i < _capCoordinates.Count; ++i)
        {
            var coordinate = _capCoordinates[i];

            var cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            cube.GetComponent<Renderer>().material = new Material(Shader.Find("Unlit/Color"));
            cube.GetComponent<Renderer>().material.color = _lineColors[i];
            cube.transform.position = coordinate;
            cube.transform.localScale = new Vector3(_scaleIndex, _scaleIndex, _scaleIndex);

            _nodes.Add(cube);
        }
    }

    void OnGUI()
    {
        if (!_showLabels) return;

        for (var i = 0; i < _capCoordinates.Count; ++i)
        {
            var pos = Camera.main.WorldToScreenPoint(_capCoordinates[i]);
            var guiStyle = new GUIStyle();
            guiStyle.normal.textColor = Color.black;
            guiStyle.fontSize = 15;
            //guiStyle.fontStyle = FontStyle.Bold;
            GUI.Label(new Rect(pos.x - 30, Screen.height - pos.y - 30, 100, 30), _capLabels[i], guiStyle);
        }
    }

    private bool isViewable = true;

    public float cameraSensitivity = 90;
    public float climbSpeed = 5;
    public float normalMoveSpeed = 10;
    public float fastMoveFactor = 10;

    private float rotationX;
    private float rotationY;

    private Vector3 pos1;
    private Vector3 pos2;
    float objectHeight = 2.0f;  // 2.0 for a cylinder, 1.0 for a cube

    private Vector3 lastPosition;

    void ClearEdges()
    {
        _edges.ForEach(e => Destroy(e));
        _arrows.ForEach(a => Destroy(a));

        _edges.Clear();
        _arrows.Clear();
    }

    void Update()
    {
        // Camera related operations
        var objPos = Camera.main.WorldToScreenPoint(Vector3.zero);

        if (Input.GetMouseButtonDown(0))
        {
            lastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            var delta = Input.mousePosition - lastPosition;
            transform.Translate(-delta.x * 1.0f, -delta.y * 1.0f, 0);
            lastPosition = Input.mousePosition;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
        {
            Camera.main.fieldOfView += 2;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            Camera.main.fieldOfView -= 2;
        }

        if (Input.GetKeyDown(KeyCode.KeypadPlus) ||
            Input.GetKeyDown(KeyCode.Plus))
        {
            if (_percentToDisplay == 1.0f) return;

            _percentToDisplay += 0.05f;

            ClearEdges();
            DrawConnections();

            _text.text = Math.Ceiling(_percentToDisplay * 100).ToString() + "%";
        }

        if (Input.GetKeyDown(KeyCode.KeypadMinus) ||
            Input.GetKeyDown(KeyCode.Minus))
        {
            if (Math.Abs(_percentToDisplay) < 0.01) return;
            _percentToDisplay -= 0.05f;

            ClearEdges();
            DrawConnections();

            _text.text = Math.Ceiling(_percentToDisplay * 100).ToString() + "%";
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            _showLabels = !_showLabels;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            _camera.MoveHorizontal(true, objPos);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            _camera.MoveHorizontal(false, objPos);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            _camera.MoveVertical(true, objPos);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            _camera.MoveVertical(false, objPos);
        }
    }
}
