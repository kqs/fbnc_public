﻿using UnityEngine;

namespace Assets
{
    public class CameraOrbit : MonoBehaviour
    {
        public Transform target;

        public float horizMove = 0.002f;
        public float vertMode = 0.002f;

        public void MoveHorizontal(bool left, Vector3 axis)
        {
            float dir = 0.2f;
            if (!left)
                dir = -0.2f;
            transform.RotateAround(target.position, Vector3.up, horizMove * dir);
        }

        public void MoveVertical(bool up, Vector3 axis)
        {
            float dir = 0.2f;
            if (!up)
                dir = -0.2f;
            transform.RotateAround(target.position, transform.TransformDirection(Vector3.right), vertMode * dir);
        }
    }
}
