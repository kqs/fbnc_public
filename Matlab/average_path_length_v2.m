
index = 0;
for i = 1:4
    for j = 1:3
        stimul = strcat('D:/Data/Results/metrics/avg_path_length_v2/apl_peaks_scaled_s', string(i), '_129_', string(j), '.txt');
        baseline = strcat('D:/Data/Results/metrics/avg_path_length_v2/apl_peaks_scaled_s', string(i), '_150_129_', string(j), '.txt');
        
        stimulContent = fscanf(fopen(stimul,'r'), '%f');
        baselineContent = fscanf(fopen(baseline,'r'), '%f');
        
        index = index + 1;
        subplot(4, 3, index);
        plot(stimulContent);
        hold on;
        plot(baselineContent);
        legend('stimulus', 'baseline');
        if j == 1
            title('SEEN');
        elseif j == 2
            title('UNCERTAIN');     
        elseif j == 3
            title('UNSEEN');
        end
        hold off;
    end
end