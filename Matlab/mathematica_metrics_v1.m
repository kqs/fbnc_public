fclose('all')
network_name = 'network_lags';
corr = 'SC';
titlex = 'Betweenness Centrality / Lags - Scaled Correlation';
subject1 = 's1';
subject2 = 's2';
subject3 = 's3';
subject4 = 's4';

bc_s1_stimul_1 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject1, '/cc_129_1.txt'),'r'), '%f');
bc_s1_stimul_2 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject1, '/cc_129_2.txt'),'r'), '%f');
bc_s1_stimul_3 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject1, '/cc_129_3.txt'),'r'), '%f');
bc_s1_stimul_11 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject1, '/cc_150_129_1.txt'),'r'), '%f');
bc_s1_stimul_22 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject1, '/cc_150_129_2.txt'),'r'), '%f');
bc_s1_stimul_33 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject1, '/cc_150_129_3.txt'),'r'), '%f');

bc_s2_stimul_1 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject2, '/cc_129_1.txt'),'r'), '%f');
bc_s2_stimul_2 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject2, '/cc_129_2.txt'),'r'), '%f');
bc_s2_stimul_3 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject2, '/cc_129_3.txt'),'r'), '%f');
bc_s2_stimul_11 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject2, '/cc_150_129_1.txt'),'r'), '%f');
bc_s2_stimul_22 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject2, '/cc_150_129_2.txt'),'r'), '%f');
bc_s2_stimul_33 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject2, '/cc_150_129_3.txt'),'r'), '%f');

bc_s3_stimul_1 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject3, '/cc_129_1.txt'),'r'), '%f');
bc_s3_stimul_2 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject3, '/cc_129_2.txt'),'r'), '%f');
bc_s3_stimul_3 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject3, '/cc_129_3.txt'),'r'), '%f');
bc_s3_stimul_11 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject3, '/cc_150_129_1.txt'),'r'), '%f');
bc_s3_stimul_22 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject3, '/cc_150_129_2.txt'),'r'), '%f');
bc_s3_stimul_33 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject3, '/cc_150_129_3.txt'),'r'), '%f');

bc_s4_stimul_1 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject4, '/cc_129_1.txt'),'r'), '%f');
bc_s4_stimul_2 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject4, '/cc_129_2.txt'),'r'), '%f');
bc_s4_stimul_3 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject4, '/cc_129_3.txt'),'r'), '%f');
bc_s4_stimul_11 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject4, '/cc_150_129_1.txt'),'r'), '%f');
bc_s4_stimul_22 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject4, '/cc_150_129_2.txt'),'r'), '%f');
bc_s4_stimul_33 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject4, '/cc_150_129_3.txt'),'r'), '%f');


% bc_s1_stimul_1 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_129_1.txt'),'r'), '%f');
% bc_s1_stimul_2 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_129_2.txt'),'r'), '%f');
% bc_s1_stimul_3 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_129_3.txt'),'r'), '%f');
% bc_s1_stimul_11 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_150_129_1.txt'),'r'), '%f');
% bc_s1_stimul_22 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_150_129_2.txt'),'r'), '%f');
% bc_s1_stimul_33 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_150_129_3.txt'),'r'), '%f');
% 
% bc_s1_stimul_1 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_129_1.txt'),'r'), '%f');
% bc_s1_stimul_2 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_129_2.txt'),'r'), '%f');
% bc_s1_stimul_3 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_129_3.txt'),'r'), '%f');
% bc_s1_stimul_11 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_150_129_1.txt'),'r'), '%f');
% bc_s1_stimul_22 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_150_129_2.txt'),'r'), '%f');
% bc_s1_stimul_33 = fscanf(fopen(strcat('D:/Data/Results/metrics/r/', network_name, '/SC/', subject, '/cc_150_129_3.txt'),'r'), '%f');

figure
subplot(4, 3, 1),
plot(bc_s1_stimul_1);
hold on;
plot(bc_s1_stimul_11);
xlabel(strcat('std.dev. (', int2str(std(bc_s1_stimul_1)), ', ', int2str(std(bc_s1_stimul_11)), ')'));
ylabel('CC');
title('SEEN (129\_1)');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');


subplot(4, 3, 2);
plot(bc_s1_stimul_2);
hold on;
plot(bc_s1_stimul_22);
xlabel(strcat('std.dev. (', int2str(std(bc_s1_stimul_2)), ', ', int2str(std(bc_s1_stimul_22)), ')'));
ylabel('CC');
title('SOMETHING (129\_2)');

legend({'stimulus', 'baseline'}, 'Location','bestoutside');

subplot(4, 3, 3);
plot(bc_s1_stimul_3);
hold on;
plot(bc_s1_stimul_33);
xlabel(strcat('std.dev. (', int2str(std(bc_s1_stimul_3)), ', ', int2str(std(bc_s1_stimul_33)), ')'));
ylabel('CC');
title('NOTHING (129\_3)');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');
%-------------------------------------------------------------------------
subplot(4, 3, 4),
plot(bc_s2_stimul_1);
hold on;
plot(bc_s2_stimul_11);
xlabel(strcat('std.dev. (', int2str(std(bc_s2_stimul_1)), ', ', int2str(std(bc_s2_stimul_11)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');


subplot(4, 3, 5);
plot(bc_s2_stimul_2);
hold on;
plot(bc_s2_stimul_22);
xlabel(strcat('std.dev. (', int2str(std(bc_s2_stimul_2)), ', ', int2str(std(bc_s2_stimul_22)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');

subplot(4, 3, 6);
plot(bc_s2_stimul_3);
hold on;
plot(bc_s2_stimul_33);
xlabel(strcat('std.dev. (', int2str(std(bc_s2_stimul_3)), ', ', int2str(std(bc_s2_stimul_33)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');
%------------------------------------------------------------------------
subplot(4, 3, 7),
plot(bc_s3_stimul_1);
hold on;
plot(bc_s3_stimul_11);
xlabel(strcat('std.dev. (', int2str(std(bc_s3_stimul_1)), ', ', int2str(std(bc_s3_stimul_11)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');


subplot(4, 3, 8);
plot(bc_s3_stimul_2);
hold on;
plot(bc_s3_stimul_22);
xlabel(strcat('std.dev. (', int2str(std(bc_s3_stimul_2)), ', ', int2str(std(bc_s3_stimul_22)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');

subplot(4, 3, 9);
plot(bc_s3_stimul_3);
hold on;
plot(bc_s3_stimul_33);
xlabel(strcat('std.dev. (', int2str(std(bc_s3_stimul_3)), ', ', int2str(std(bc_s3_stimul_33)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');
%------------------------------------------------------------------------
subplot(4, 3, 10),
plot(bc_s4_stimul_1);
hold on;
plot(bc_s4_stimul_11);
xlabel(strcat('std.dev. (', int2str(std(bc_s4_stimul_1)), ', ', int2str(std(bc_s4_stimul_11)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');


subplot(4, 3, 11);
plot(bc_s4_stimul_2);
hold on;
plot(bc_s4_stimul_22);
xlabel(strcat('std.dev. (', int2str(std(bc_s4_stimul_2)), ', ', int2str(std(bc_s4_stimul_22)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside');

subplot(4, 3, 12);
plot(bc_s4_stimul_3);
hold on;
plot(bc_s4_stimul_33);
xlabel(strcat('std.dev. (', int2str(std(bc_s4_stimul_3)), ', ', int2str(std(bc_s4_stimul_33)), ')'));
ylabel('CC');
legend({'stimulus', 'baseline'}, 'Location','bestoutside'); 
%------------------------------------------------------------------------
% subplot(5, 3, 13);
% plot(lbc_s1_stimul_1);
% hold on;
% plot(lbc_s1_stimul_11);
% xlabel('std.dev.');
% ylabel('LCC');
% legend({'stimulus', 'baseline'}, 'Location','bestoutside');
% 
% subplot(5, 3, 14);
% plot(lbc_s1_stimul_2);
% hold on;
% plot(lbc_s1_stimul_22);
% xlabel('std.dev.');
% ylabel('LCC');
% legend({'stimulus', 'baseline'}, 'Location','bestoutside');
% 
% subplot(5, 3, 15);
% plot(lbc_s1_stimul_3);
% hold on;
% plot(lbc_s1_stimul_33);
% xlabel('std.dev.');
% ylabel('LCC');
% legend({'stimulus', 'baseline'}, 'Location','bestoutside');

suptitle(titlex)