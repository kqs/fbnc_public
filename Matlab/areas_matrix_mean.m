network1 = 'd:/Data/Results_last/network_peaks/s1/';

corrType = 'SC';
stimulus = '129_1';
baseline = '129_2';
baseline1 = '129_3';
nameoutput = 'SC_areas_mean_matrix_129_1';

f1= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/occipital_frontal.txt'), 'r'), '%f');
f2= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/occipital_parietal.txt'), 'r'), '%f');
f3= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/occipital_leftTemporal.txt'), 'r'), '%f');
f4= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/occipital_rightTemporal.txt'), 'r'), '%f');
f5= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/frontal_parietal.txt'), 'r'), '%f');
f6= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/frontal_leftTemporal.txt'), 'r'), '%f');
f7= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/frontal_rightTemporal.txt'), 'r'), '%f');
f8= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/parietal_leftTemporal.txt'), 'r'), '%f');
f9= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/parietal_rightTemporal.txt'), 'r'), '%f');
f10= fscanf(fopen(strcat(network1, '/' , corrType,  '/', stimulus, '/leftTemporal_rightTemporal.txt'), 'r'), '%f');

f1baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/occipital_frontal.txt'), 'r'), '%f');
f2baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/occipital_parietal.txt'), 'r'), '%f');
f3baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/occipital_leftTemporal.txt'), 'r'), '%f');
f4baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/occipital_rightTemporal.txt'), 'r'), '%f');
f5baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/frontal_parietal.txt'), 'r'), '%f');
f6baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/frontal_leftTemporal.txt'), 'r'), '%f');
f7baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/frontal_rightTemporal.txt'), 'r'), '%f');
f8baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/parietal_leftTemporal.txt'), 'r'), '%f');
f9baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/parietal_rightTemporal.txt'), 'r'), '%f');
f10baseline = fscanf(fopen(strcat(network1, '/' , corrType,  '/', baseline, '/leftTemporal_rightTemporal.txt'), 'r'), '%f');

f11= fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/occipital_frontal.txt'), 'r'), '%f');
f22 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/occipital_parietal.txt'), 'r'), '%f');
f33 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/occipital_leftTemporal.txt'), 'r'), '%f');
f44 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/occipital_rightTemporal.txt'), 'r'), '%f');
f55 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/frontal_parietal.txt'), 'r'), '%f');
f66 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/frontal_leftTemporal.txt'), 'r'), '%f');
f77 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/frontal_rightTemporal.txt'), 'r'), '%f');
f88 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/parietal_leftTemporal.txt'), 'r'), '%f');
f99 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/parietal_rightTemporal.txt'), 'r'), '%f');
f100 = fscanf(fopen(strcat(network1, '/' , corrType,  '/', baseline1, '/leftTemporal_rightTemporal.txt'), 'r'), '%f');

f1mean = mean(f1);
f2mean = mean(f2);
f3mean = mean(f3);
f4mean = mean(f4);
f5mean = mean(f5);
f6mean = mean(f6);
f7mean = mean(f7);
f8mean = mean(f8);
f9mean = mean(f9);
f10mean = mean(f10);

f1bmean = mean(f1baseline);
f2bmean = mean(f2baseline);
f3bmean = mean(f3baseline);
f4bmean = mean(f4baseline);
f5bmean = mean(f5baseline);
f6bmean = mean(f6baseline);
f7bmean = mean(f7baseline);
f8bmean = mean(f8baseline);
f9bmean = mean(f9baseline);
f10bmean = mean(f10baseline);

f11mean = mean(f11);
f22mean = mean(f22);
f33mean = mean(f33);
f44mean = mean(f44);
f55mean = mean(f55);
f66mean = mean(f66);
f77mean = mean(f77);
f88mean = mean(f88);
f99mean = mean(f99);
f100mean = mean(f100);

aline = [f1mean, f2mean, f3mean, f4mean, f5mean, f6mean, f7mean, f8mean, f9mean, f10mean];
bline = [f1bmean, f2bmean, f3bmean, f4bmean, f5bmean, f6bmean, f7bmean, f8bmean, f9bmean, f10bmean];
cline = [f11mean, f22mean, f33mean, f44mean, f55mean, f66mean, f77mean, f88mean, f99mean, f100mean];

a = [0 f1mean f2mean f3mean f4mean;
     f1mean 0 f5mean f6mean f7mean;
     f2mean f5mean 0 f8mean f9mean;
     f3mean f6mean f8mean 0 f10mean;
     f4mean f7mean f9mean f10mean 0];
 
 b = [0 f1bmean f2bmean f3bmean f4bmean;
     f1bmean 0 f5bmean f6bmean f7bmean;
     f2bmean f5bmean 0 f8bmean f9bmean;
     f3bmean f6bmean f8bmean 0 f10bmean;
     f4bmean f7bmean f9bmean f10bmean 0];
 
  c = [0 f11mean f22mean f33mean f44mean;
     f11mean 0 f55mean f66mean f77mean;
     f22mean f55mean 0 f88mean f99mean;
     f33mean f66mean f88mean 0 f100mean;
     f44mean f77mean f99mean f100mean 0];

fclose('all');
subplot(1,3,1);
imagesc(a);     
colormap parula

colorbar
title('SCALED CORRELATION - AREAS - [129,3] - mean matrix');
set(gca, 'xtick', [1,2,3,4,5]);
set(gca, 'XTickLabel', {'occipital','frontal','parietal','left temp','right temp'});
set(gca, 'ytick', [1,2,3,4,5]);
set(gca, 'YTickLabel', {'occipital','frontal','parietal','left temp','right temp'});
hold on
subplot(1,3,2);
imagesc(b);     
colormap parula

colorbar
title('SCALED CORRELATION - AREAS - [129,3] - mean matrix');
set(gca, 'xtick', [1,2,3,4,5]);
set(gca, 'XTickLabel', {'occipital','frontal','parietal','left temp','right temp'});
set(gca, 'ytick', [1,2,3,4,5]);
set(gca, 'YTickLabel', {'occipital','frontal','parietal','left temp','right temp'});
hold on
subplot(1,3,3);
imagesc(c);     
colormap parula

colorbar
title('SCALED CORRELATION - AREAS - [129,3] - mean matrix');
set(gca, 'xtick', [1,2,3,4,5]);
set(gca, 'XTickLabel', {'occipital','frontal','parietal','left temp','right temp'});
set(gca, 'ytick', [1,2,3,4,5]);
set(gca, 'YTickLabel', {'occipital','frontal','parietal','left temp','right temp'});


print(nameoutput,'-dpdf')
