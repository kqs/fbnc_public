corrType = 'SC';
stimulus = '129_1';
baseline = '150_129_1';
baseline1 = '129_3';
nameoutput = 'SC_areas_129_1';

subjNb = 10;
index = 1;
tmp1 = zeros(10, subjNb);
tmp2 = zeros(10, subjNb);
tmp3 = zeros(10, subjNb);
for i = 1:subjNb+1
    if i == 5
        continue; 
    end
network1 = strcat('D:/Data2/Results_last_negative/network_peaks/s', int2str(i) ,'/');
%network1 = strcat('d:/Data/Results/networks_negative_weights/network_pearson/s', int2str(i) ,'/');
f1= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/occipital_frontal.txt'), 'r'), '%f');
f2= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/occipital_parietal.txt'), 'r'), '%f');
f3= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/occipital_leftTemporal.txt'), 'r'), '%f');
f4= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/occipital_rightTemporal.txt'), 'r'), '%f');
f5= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/frontal_parietal.txt'), 'r'), '%f');
f6= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/frontal_leftTemporal.txt'), 'r'), '%f');
f7= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/frontal_rightTemporal.txt'), 'r'), '%f');
f8= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/parietal_leftTemporal.txt'), 'r'), '%f');
f9= fscanf(fopen(strcat(network1, '/' , corrType, '/', stimulus, '/parietal_rightTemporal.txt'), 'r'), '%f');
f10= fscanf(fopen(strcat(network1, '/' , corrType,  '/', stimulus, '/leftTemporal_rightTemporal.txt'), 'r'), '%f');

f1baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/occipital_frontal.txt'), 'r'), '%f');
f2baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/occipital_parietal.txt'), 'r'), '%f');
f3baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/occipital_leftTemporal.txt'), 'r'), '%f');
f4baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/occipital_rightTemporal.txt'), 'r'), '%f');
f5baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/frontal_parietal.txt'), 'r'), '%f');
f6baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/frontal_leftTemporal.txt'), 'r'), '%f');
f7baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/frontal_rightTemporal.txt'), 'r'), '%f');
f8baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/parietal_leftTemporal.txt'), 'r'), '%f');
f9baseline = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline, '/parietal_rightTemporal.txt'), 'r'), '%f');
f10baseline = fscanf(fopen(strcat(network1, '/' , corrType,  '/', baseline, '/leftTemporal_rightTemporal.txt'), 'r'), '%f');

f11= fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/occipital_frontal.txt'), 'r'), '%f');
f22 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/occipital_parietal.txt'), 'r'), '%f');
f33 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/occipital_leftTemporal.txt'), 'r'), '%f');
f44 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/occipital_rightTemporal.txt'), 'r'), '%f');
f55 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/frontal_parietal.txt'), 'r'), '%f');
f66 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/frontal_leftTemporal.txt'), 'r'), '%f');
f77 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/frontal_rightTemporal.txt'), 'r'), '%f');
f88 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/parietal_leftTemporal.txt'), 'r'), '%f');
f99 = fscanf(fopen(strcat(network1, '/' , corrType, '/', baseline1, '/parietal_rightTemporal.txt'), 'r'), '%f');
f100 = fscanf(fopen(strcat(network1, '/' , corrType,  '/', baseline1, '/leftTemporal_rightTemporal.txt'), 'r'), '%f');

f1mean = mean(f1);
f2mean = mean(f2);
f3mean = mean(f3);
f4mean = mean(f4);
f5mean = mean(f5);
f6mean = mean(f6);
f7mean = mean(f7);
f8mean = mean(f8);
f9mean = mean(f9);
f10mean = mean(f10);

f1bmean = mean(f1baseline);
f2bmean = mean(f2baseline);
f3bmean = mean(f3baseline);
f4bmean = mean(f4baseline);
f5bmean = mean(f5baseline);
f6bmean = mean(f6baseline);
f7bmean = mean(f7baseline);
f8bmean = mean(f8baseline);
f9bmean = mean(f9baseline);
f10bmean = mean(f10baseline);

f11mean = mean(f11);
f22mean = mean(f22);
f33mean = mean(f33);
f44mean = mean(f44);
f55mean = mean(f55);
f66mean = mean(f66);
f77mean = mean(f77);
f88mean = mean(f88);
f99mean = mean(f99);
f100mean = mean(f100);

tmp1(1,i) = f1mean; 
tmp1(2,i) = f2mean; 
tmp1(3,i) = f3mean; 
tmp1(4,i) = f4mean; 
tmp1(5,i) = f5mean;
tmp1(6,i) = f6mean; 
tmp1(7,i) = f7mean; 
tmp1(8,i) = f8mean; 
tmp1(9,i) = f9mean; 
tmp1(10,i) = f10mean; 

tmp2(1,i) = f1bmean; 
tmp2(2,i) = f2bmean; 
tmp2(3,i) = f3bmean; 
tmp2(4,i) = f4bmean; 
tmp2(5,i) = f5bmean;
tmp2(6,i) = f6bmean; 
tmp2(7,i) = f7bmean; 
tmp2(8,i) = f8bmean; 
tmp2(9,i) = f9bmean; 
tmp2(10,i) = f10bmean; 

tmp3(1,i) = f11mean; 
tmp3(2,i) = f22mean; 
tmp3(3,i) = f33mean; 
tmp3(4,i) = f44mean; 
tmp3(5,i) = f55mean;
tmp3(6,i) = f66mean; 
tmp3(7,i) = f77mean; 
tmp3(8,i) = f88mean; 
tmp3(9,i) = f99mean; 
tmp3(10,i) = f100mean; 
aline = [f1mean, f2mean, f3mean, f4mean, f5mean, f6mean, f7mean, f8mean, f9mean, f10mean];
bline = [f1bmean, f2bmean, f3bmean, f4bmean, f5bmean, f6bmean, f7bmean, f8bmean, f9bmean, f10bmean];
cline = [f11mean, f22mean, f33mean, f44mean, f55mean, f66mean, f77mean, f88mean, f99mean, f100mean];

fclose('all');
% subplot(1, subjNb, index);
% p1 = stem(aline);
% p1.Marker = 'o';
% p1.Color = 'blue';
% p1.MarkerFaceColor = 'blue';
% set(gca, 'xtick', [1,2,3,4,5, 6, 7, 8, 9, 10]);
% set(gca, 'XTickLabel', {'O-F','O-P','O-LT','O-RT','F-P', 'F-LT', 'F-RT', 'P-LT', 'P-RT', 'LT-RT'});
% hold on
% p2 = stem(bline);
% p2.Marker = 's';
% p2.Color = 'red';
% p2.MarkerFaceColor = 'red';
% set(gca, 'xtick', [1,2,3,4,5, 6, 7, 8, 9, 10]);
% set(gca, 'XTickLabel', {'O-F','O-P','O-LT','O-RT','F-P', 'F-LT', 'F-RT', 'P-LT', 'P-RT', 'LT-RT'});
%  hold on
%  p3 = stem(cline);
%  p3.Marker = 'd';
%  p3.Color = 'black';
%  p3.MarkerFaceColor = 'black';
%  set(gca, 'xtick', [1,2,3,4,5, 6, 7, 8, 9, 10]);
%  set(gca, 'XTickLabel', {'O-F','O-P','O-LT','O-RT','F-P', 'F-LT', 'F-RT', 'P-LT', 'P-RT', 'LT-RT'});
%  hold on;

index = index + 1;
end

tt1 = zeros(1, 10);
tt2 = zeros(1, 10);
tt3 = zeros(1, 10);

tt1(1) = mean(tmp1(1,:)); tt1(2) = mean(tmp1(2,:)); tt1(3) = mean(tmp1(3,:)); tt1(4) = mean(tmp1(4,:)); tt1(5) = mean(tmp1(5,:));
tt1(6) = mean(tmp1(6,:)); tt1(7) = mean(tmp1(7,:)); tt1(8) = mean(tmp1(8,:)); tt1(9) = mean(tmp1(9,:)); tt1(10) = mean(tmp1(10,:));

tt2(1) = mean(tmp2(1,:)); tt2(2) = mean(tmp2(2,:)); tt2(3) = mean(tmp2(3,:)); tt2(4) = mean(tmp2(4,:)); tt2(5) = mean(tmp2(5,:));
tt2(6) = mean(tmp2(6,:)); tt2(7) = mean(tmp2(7,:)); tt2(8) = mean(tmp2(8,:)); tt2(9) = mean(tmp2(9,:)); tt2(10) = mean(tmp2(10,:));

tt3(1) = mean(tmp3(1,:)); tt3(2) = mean(tmp3(2,:)); tt3(3) = mean(tmp3(3,:)); tt3(4) = mean(tmp3(4,:)); tt3(5) = mean(tmp3(5,:));
tt3(6) = mean(tmp3(6,:)); tt3(7) = mean(tmp3(7,:)); tt3(8) = mean(tmp3(8,:)); tt3(9) = mean(tmp3(9,:)); tt3(10) = mean(tmp3(10,:));

 p1 = plot(tt1);
 p1.Marker = 'o';
 p1.Color = 'blue';
 p1.MarkerFaceColor = 'blue';
 set(gca, 'xtick', [1,2,3,4,5, 6, 7, 8, 9, 10]);
 set(gca, 'XTickLabel', {'O-F','O-P','O-LT','O-RT','F-P', 'F-LT', 'F-RT', 'P-LT', 'P-RT', 'LT-RT'});
 hold on
 p2 = plot(tt2);
 p2.Marker = 's';
 p2.Color = 'red';
 p2.MarkerFaceColor = 'red';
 set(gca, 'xtick', [1,2,3,4,5, 6, 7, 8, 9, 10]);
 set(gca, 'XTickLabel', {'O-F','O-P','O-LT','O-RT','F-P', 'F-LT', 'F-RT', 'P-LT', 'P-RT', 'LT-RT'});
 set(gca, 'FontSize', 20)
 hold on
%   p3 = plot(tt3);
%   p3.Marker = 'd';
%   p3.Color = 'black';
%   p3.MarkerFaceColor = 'black';
%   set(gca, 'xtick', [1,2,3,4,5, 6, 7, 8, 9, 10]);
%   set(gca, 'XTickLabel', {'O-F','O-P','O-LT','O-RT','F-P', 'F-LT', 'F-RT', 'P-LT', 'P-RT', 'LT-RT'});


legend({'STIMULUS', 'BASELINE'}, 'Location','bestoutside');
%print(nameoutput,'-dpdf')
