network1 = 'NETWORK_1_PEAKS_WEIGHT';
network2 = 'NETWORK_2_LAGS_WEIGHT';
subject = 's1';
stimulus = '129_1';
prefix = 'assort';

f1 = strcat(network1, '/' , subject, '/CC/metrics/', prefix, '_129_1.txt');
f2 = strcat(network1, '/' , subject, '/CC/metrics/', prefix, '_129_2.txt');
f3 = strcat(network1, '/' , subject, '/CC/metrics/', prefix, '_129_3.txt');
f4 = strcat(network1, '/' , subject, '/SC/metrics/', prefix, '_129_1.txt');
f5 = strcat(network1, '/' , subject, '/SC/metrics/', prefix, '_129_2.txt');
f6 = strcat(network1, '/' , subject, '/SC/metrics/', prefix, '_129_3.txt');

f7 = strcat(network2, '/' , subject, '/CC/metrics/', prefix, '_129_1.txt');
f8 = strcat(network2, '/' , subject, '/CC/metrics/', prefix, '_129_2.txt');
f9 = strcat(network2, '/' , subject, '/CC/metrics/', prefix, '_129_3.txt');
f10 = strcat(network2, '/' , subject, '/SC/metrics/', prefix, '_129_1.txt');
f11 = strcat(network2, '/' , subject, '/SC/metrics/', prefix, '_129_2.txt');
f12 = strcat(network2, '/' , subject, '/SC/metrics/', prefix, '_129_3.txt');


 gcc1 = mean(fscanf(fopen(f1,'r'), '%f'));
 gcc2 = mean(fscanf(fopen(f2,'r'), '%f'));
 gcc3 = mean(fscanf(fopen(f3,'r'), '%f'));
 gcc4 = mean(fscanf(fopen(f4,'r'), '%f'));
 gcc5 = mean(fscanf(fopen(f5,'r'), '%f'));
 gcc6 = mean(fscanf(fopen(f6,'r'), '%f'));
 
 gcc7 = mean(fscanf(fopen(f7,'r'), '%f'));
 gcc8 = mean(fscanf(fopen(f8,'r'), '%f'));
 gcc9 = mean(fscanf(fopen(f9,'r'), '%f'));
 gcc10 = mean(fscanf(fopen(f10,'r'), '%f'));
 gcc11 = mean(fscanf(fopen(f11,'r'), '%f'));
 gcc12 = mean(fscanf(fopen(f12,'r'), '%f'));
 
 data = [gcc1 gcc2 gcc3];
 data2 = [gcc4 gcc5 gcc6];
 
 data3 = [gcc7 gcc8 gcc9];
 data4 = [gcc10 gcc11 gcc12];
 
subplot(2, 2, 1);
b = bar(diag(data),'stacked');
ax = legend(b, {'seen', 'uncertain', 'nothing'});
title('Peaks Network - Cross-Correlation');


subplot(2, 2, 2);
b = bar(diag(data2),'stacked');
ax = legend(b, {'seen', 'uncertain', 'nothing'});
title('Peaks Network - Scaled-Correlation');

subplot(2, 2, 3);
b = bar(diag(data3),'stacked');
ax = legend(b, {'seen', 'uncertain', 'nothing'});
title('Lags Network - Cross-Correlation');

subplot(2, 2, 4);
b = bar(diag(data4),'stacked');
ax = legend(b, {'seen', 'uncertain', 'nothing'});
title('Lags Network - Scaled-Correlation');