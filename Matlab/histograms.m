
index = 1;
for i = 1:4

f1=strcat('d:/Data/Results/networks_negative_weights/network_peaks/s', int2str(i), '/CC/150_129_1/average.txt');
f2=strcat('d:/Data/Results/networks_negative_weights/network_peaks/s', int2str(i), '/CC/129_1/average.txt');

% f3='difference/diff_150_129_1_SC.txt';

% f4='subject1/SC_150_129_2/mean_matrix.txt';
% f5='subject1/SC_129_2/mean_matrix.txt';
% f6='difference/diff_150_129_2_SC.txt';
% 
% f7='subject1/SC_150_129_3/mean_matrix.txt';
% f8='subject1/SC_129_3/mean_matrix.txt';
% f9='difference/diff_150_129_3_SC.txt';

F1 = dlmread(f1);
F2 = dlmread(f2);
F3 = F2 - F1;
% F3 = dlmread(f3);

% F4 = dlmread(f4);
% F5 = dlmread(f5);
% F6 = dlmread(f6);
% 
% F7 = dlmread(f7);
% F8 = dlmread(f8);
% F9 = dlmread(f9);

subplot(4, 3, index);
hist(F1);
title('SC - [150, 129, 1]')
set(get(gca,'child'),'FaceColor','none','EdgeColor','k');
hold on
subplot(4, 3, index+1);
hist(F2);
title('SC - [129, 1]')
set(get(gca,'child'),'FaceColor','none','EdgeColor','k');
hold on
subplot(4, 3, index+2);
hist(F3);
title('DIFERENTA')
set(get(gca,'child'),'FaceColor','none','EdgeColor','k');
hold on

index = index + 3;
% subplot(3, 3, 4);
% hist(F4);
% title('SC - [150, 129, 2]')
% hold on
% subplot(3, 3, 5);
% hist(F5);
% title('SC - [129, 2]')
% hold on
% subplot(3, 3, 6);
% hist(F6);
% title('DIFERENTA')
% hold on
% subplot(3, 3, 7);
% hist(F7);
% title('SC - [150, 129, 3]')
% hold on
% subplot(3, 3, 8);
% hist(F8);
% title('SC - [129, 3]')
% hold on
% subplot(3, 3, 9);
% hist(F9);
% title('DIFERENTA')

end