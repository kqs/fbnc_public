f1='d:/Data/Results/networks_negative_weights/network_peaks/s1/SC/150_129_3/average.txt';
f2='d:/Data/Results/networks_negative_weights/network_peaks/s1/SC/129_3/average.txt';
% f3='difference/diff_150_129_3_CC.txt';

% f4='scaled_150_129_3/mean_matrix.txt';
% f5='scaled_129_3/mean_matrix.txt';
% f6='difference/diff_150_129_3_SC.txt';

A = dlmread(f1);
B = dlmread(f2);
C = B - A;

% D = dlmread(f4);
% E = dlmread(f5);
% F = dlmread(f6);

subplot(1, 3, 1);
imagesc(A);
colormap gray
colorbar
title('[1] S1 / PEAKS / SC / 150\_129\_1')
hold on
subplot(1,3,2);
imagesc(B);
colormap gray
colorbar
title('[2] S1 / PEAKS / SC / 129\_1')
hold on
subplot(1,3,3);
imagesc(C);
colormap gray
colorbar
title('DIFF [2] - [1]')

% hold on
% subplot(2, 3, 4);
% imagesc(D);
% colormap jet
% colorbar
% title('POZA 1: SC - [150, 129, 3]')
% hold on
% subplot(2,3,5);
% imagesc(E);
% colormap jet
% colorbar
% title('POZA 1: SC - [150, 129, 3]')
% hold on
% subplot(2,3,6);
% imagesc(F);
% colormap jet
% colorbar
% title('POZA 2 - POZA 1')