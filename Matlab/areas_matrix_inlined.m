network1 = 'NETWORK_2_LAGS_WEIGHT';
network2 = 'NETWORK_2_LAGS_WEIGHT';
network3 = 'NETWORK_3_PEARSON';

corrType = 'SC';
baseline = '150_129_1';
stimulus = '129_1';
nameoutput = 'SC_areas_mean_129_1';

f1baseline = strcat(network1, '/' , corrType, '/', baseline, '/occipital_frontal_150_129_inlined.txt');
f2baseline = strcat(network1, '/' , corrType, '/', baseline, '/occipital_parietal_150_129_inlined.txt');
f3baseline = strcat(network1, '/' , corrType, '/', baseline, '/occipital_leftTemporal_150_129_inlined.txt');
f4baseline = strcat(network1, '/' , corrType, '/', baseline, '/occipital_rightTemporal_150_129_inlined.txt');
f5baseline = strcat(network1, '/' , corrType, '/', baseline, '/frontal_parietal_150_129_inlined.txt');
f6baseline = strcat(network1, '/' , corrType, '/', baseline, '/frontal_leftTemporal_150_129_inlined.txt');
f7baseline = strcat(network1, '/' , corrType, '/', baseline, '/frontal_rightTemporal_150_129_inlined.txt');
f8baseline = strcat(network1, '/' , corrType, '/', baseline, '/parietal_leftTemporal_150_129_inlined.txt');
f9baseline = strcat(network1, '/' , corrType, '/', baseline, '/parietal_rightTemporal_150_129_inlined.txt');
f10baseline = strcat(network1, '/' , corrType,  '/', baseline, '/leftTemporal_rightTemporal_150_129_inlined.txt');

f1= strcat(network1, '/' , corrType, '/', stimulus, '/occipital_frontal_', stimulus, '_inlined.txt');
f2= strcat(network1, '/' , corrType, '/', stimulus, '/occipital_parietal_', stimulus, '_inlined.txt');
f3= strcat(network1, '/' , corrType, '/', stimulus, '/occipital_leftTemporal_', stimulus, '_inlined.txt');
f4= strcat(network1, '/' , corrType, '/', stimulus, '/occipital_rightTemporal_', stimulus, '_inlined.txt');
f5= strcat(network1, '/' , corrType, '/', stimulus, '/frontal_parietal_', stimulus, '_inlined.txt');
f6= strcat(network1, '/' , corrType, '/', stimulus, '/frontal_leftTemporal_', stimulus, '_inlined.txt');
f7= strcat(network1, '/' , corrType, '/', stimulus, '/frontal_rightTemporal_', stimulus, '_inlined.txt');
f8= strcat(network1, '/' , corrType, '/', stimulus, '/parietal_leftTemporal_', stimulus, '_inlined.txt');
f9= strcat(network1, '/' , corrType, '/', stimulus, '/parietal_rightTemporal_', stimulus, '_inlined.txt');
f10= strcat(network1, '/' , corrType,  '/', stimulus, '/leftTemporal_rightTemporal_', stimulus, '_inlined.txt');

F1 = fscanf(fopen(f1,'r'), '%f');
F1baseline = fscanf(fopen(f1baseline,'r'), '%f');

F2 = fscanf(fopen(f2,'r'), '%f');
F2baseline = fscanf(fopen(f2baseline,'r'), '%f');

F3 = fscanf(fopen(f3,'r'), '%f');
F3baseline = fscanf(fopen(f3baseline,'r'), '%f');

F4 = fscanf(fopen(f4,'r'), '%f');
F4baseline = fscanf(fopen(f4baseline,'r'), '%f');

F5 = fscanf(fopen(f5,'r'), '%f');
F5baseline = fscanf(fopen(f5baseline,'r'), '%f');

F6 = fscanf(fopen(f6,'r'), '%f');
F6baseline = fscanf(fopen(f6baseline,'r'), '%f');

F7 = fscanf(fopen(f7,'r'), '%f');
F7baseline = fscanf(fopen(f7baseline,'r'), '%f');

F8 = fscanf(fopen(f8,'r'), '%f');
F8baseline = fscanf(fopen(f8baseline,'r'), '%f');

F9 = fscanf(fopen(f9,'r'), '%f');
F9baseline = fscanf(fopen(f9baseline,'r'), '%f');

F10 = fscanf(fopen(f10,'r'), '%f');
F10baseline = fscanf(fopen(f10baseline,'r'), '%f');

subplot(10, 1, 1);
plot(F1);
title('OCCIPITAL - FRONTAL');
hold on
plot(F1baseline);
legend('stimulus', 'baseline');

subplot(10, 1, 2);
plot(F2);
title('OCCIPITAL - PARIETAL');
hold on
plot(F2baseline);
legend('stimulus', 'baseline');

subplot(10, 1, 3);
plot(F3);
title('OCCIPITAL - LEFT.TEMPORAL');
hold on
plot(F3baseline);
legend('stimulus', 'baseline');

subplot(10, 1, 4);
plot(F4);
title('OCCIPITAL - RIGHT.TEMPORAL');
hold on
plot(F4baseline);
legend('stimulus', 'baseline');


subplot(10, 1, 5);
plot(F5);
title('FRONTAL - PARIETAL');
hold on
plot(F5baseline);
legend('stimulus', 'baseline');

subplot(10, 1, 6);
plot(F6);
title('FRONTAL - LEFT.TEMPORAL');
hold on
plot(F6baseline);
legend('stimulus', 'baseline');


subplot(10, 1, 7);
plot(F7);
title('FRONTAL - RIGHT.TEMPORAL');
hold on
plot(F7baseline);
legend('stimulus', 'baseline');


subplot(10, 1, 8);
plot(F8);
title('PARIETAL - LEFT.TEMPORAL');
hold on
plot(F8baseline);
legend('stimulus', 'baseline');


subplot(10, 1, 9);
plot(F9);
title('PARIETAL - RIGHT.TEMPORAL');
hold on
plot(F9baseline);
legend('stimulus', 'baseline');

subplot(10, 1, 10);
plot(F10);
title('LEFT.TEMPORAL - RIGHT.TEMPORAL');
hold on
plot(F10baseline);
legend('stimulus', 'baseline');

print(nameoutput,'-dpdf','-fillpage')
%
%print -dsvg ee.svg
%
