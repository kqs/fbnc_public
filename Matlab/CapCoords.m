channel_names = {'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15', 'A16', 'A17', 'A18', 'A19', 'A20', 'A21', 'A22', 'A23', 'A24', 'A25', 'A26', 'A27', 'A28', 'A29', 'A30', 'A31', 'A32', 'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'B13', 'B14', 'B15', 'B16', 'B17', 'B18', 'B19', 'B20', 'B21', 'B22', 'B23', 'B24', 'B25', 'B26', 'B27', 'B28', 'B29', 'B30', 'B31', 'B32', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12', 'C13', 'C14', 'C15', 'C16', 'C17', 'C18', 'C19', 'C20', 'C21', 'C22', 'C23', 'C24', 'C25', 'C26', 'C27', 'C28', 'C29', 'C30', 'C31', 'C32', 'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11', 'D12', 'D13', 'D14', 'D15', 'D16', 'D17', 'D18', 'D19', 'D20', 'D21', 'D22', 'D23', 'D24', 'D25', 'D26', 'D27', 'D28', 'D29', 'D30', 'D31', 'D32'};
xs = [0, 17, 34, 50, 58, 45, 52, 66, 70, 71, 69, 64, 75, 81, 83, 82, 78, 68, 63, 74, 82, 86, 87, 85, 79, 75, 81, 83, 82, 78, 68, 58, 5, 24, 45, 52, 66, 70, 71, 69, 64, 50, 51, 51, 48, 27, 27, 25, 28, 24, 25, 0, 0, 0, 0, 0, 0, 0, -27, -27, -25, -28, -24, -25, -14, -24, -45, -52, -48, -51, -51, -71, -70, -66, -43, -58, -68, -78, -82, -83, -87, -86, -82, -74, -63, -50, -34, -43, -58, -68, -78, -82, -83, -71, -70, -66, -14, -24, -45, -52, -48, -51, -51, -27, -27, -25, -28, -24, -25, 0, 5, 24, 25, 0, 0, 0, 0, 0, 0, 27, 27, 25, 28, 24, 48, 51, 51, 50];
ys = [0, 0, 0, 0, -24, -45, -52, -48, -51, -51, -50, -47, -25, -26, -27, -27, -25, -28, 0, 0, 0, 0, 0, 0, 0, 25, 26, 27, 27, 25, 28, 24, 17, 24, 45, 52, 48, 51, 51, 50, 47, 69, 71, 70, 66, 83, 82, 78, 68, 58, 43, 34, 50, 63, 74, 82, 86, 87, 83, 82, 78, 68, 58, 43, 10, 24, 45, 52, 66, 70, 71, 51, 51, 48, 25, 24, 28, 25, 27, 27, 0, 0, 0, 0, 0, 0, 0, -25, -24, -28, -25, -27, -27, -51, -51, -48, -10, -24, -45, -52, -66, -70, -71, -83, -82, -78, -68, -58, -43, -34, -17, -24, -43, -50, -63, -74, -82, -86, -87, -83, -82, -78, -68, -58, -66, -70, -71, -69];
zs = [88, 86, 81, 72, 61, 61, 47, 31, 14, -3, -20, -37, -37, -20, -3, 14, 31, 47, 61, 47, 31, 14, -3, -20, -37, -37, -20, -3, 14, 31, 47, 61, 86, 81, 61, 47, 31, 14, -3, -20, -37, -20, -3, 14, 31, -3, 14, 31, 47, 61, 72, 81, 72, 61, 47, 31, 14, -3, -3, 14, 31, 47, 61, 72, 86, 81, 61, 47, 31, 14, -3, -3, 14, 31, 72, 61, 47, 31, 14, -3, -3, 14, 31, 47, 61, 72, 81, 72, 61, 47, 31, 14, -3, -3, 14, 31, 86, 81, 61, 47, 31, 14, -3, -3, 14, 31, 47, 61, 72, 81, 86, 81, 72, 72, 61, 47, 31, 14, -3, -3, 14, 31, 47, 61, 31, 14, -3, -20];

fileSource = fopen('source_output.txt', 'r');
fileDest = fopen('destination_output.txt', 'r');
fileWeight = fopen('weight_output.txt', 'r');
source = fscanf(fileSource, '%f');
dest = fscanf(fileDest, '%f');
weight = fscanf(fileWeight, '%f');

connections = length(source);
source_points_x=NaN*ones(connections);
source_points_y=NaN*ones(connections);
source_points_z=NaN*ones(connections);

source_points_r=NaN*ones(connections);
source_points_g=NaN*ones(connections);
source_points_b=NaN*ones(connections);

channel_colors_r = NaN*ones(128);
channel_colors_g = NaN*ones(128);
channel_colors_b = NaN*ones(128);

for i = 1:128
    channel_colors_r(i) = rand(1);
    channel_colors_g(i) = rand(1);
    channel_colors_b(i) = rand(1);
end

for i = 1:connections
    elem = source(i);
    source_points_x(i) = xs(elem);
    source_points_y(i) = ys(elem);
    source_points_z(i) = zs(elem);
    source_points_r(i) = channel_colors_r(elem);
    source_points_g(i) = channel_colors_g(elem);
    source_points_b(i) = channel_colors_b(elem);
end

connections = length(dest);
dest_points_x=NaN*ones(connections);
dest_points_y=NaN*ones(connections);
dest_points_z=NaN*ones(connections);

for i = 1:connections
    elem = dest(i);
    dest_points_x(i) = xs(elem);
    dest_points_y(i) = ys(elem);
    dest_points_z(i) = zs(elem);
end

delta_x = dest_points_x - source_points_x;
delta_y = dest_points_y - source_points_y;
delta_z = dest_points_z - source_points_z;

figure
plot3(xs,ys,zs,'r*');
for i = 1:connections
    %q = quiver3(source_points_x(i), source_points_y(i), source_points_z(i), delta_x(i), delta_y(i), delta_z(i),0);
    %q.Color = [source_points_r(i) source_points_g(i) source_points_b(i)];
    %q.AutoScale = 'off';
    %q.AlignVertexCenters='on';
    %q.DisplayName=num2str(weight(i));
    %q.Tag=num2str(weight(i));
    mArrow3([source_points_x(i) source_points_y(i) source_points_z(i)], [dest_points_x(i) dest_points_y(i) dest_points_z(i)], 'facealpha', 1, 'color', [source_points_r(i) source_points_g(i) source_points_b(i)], 'stemWidth', 0.1);
    hold on
end
legend
text(xs,ys,zs,channel_names)
