network1 = 'NETWORK_1_PEAKS_WEIGHT';
network2 = 'NETWORK_2_LAGS_WEIGHT';
network3 = 'NETWORK_3_PEARSON';

corrType = 'CC';

f1= strcat(network1, '/' , corrType, '/129_1/occipital_frontal_129_1.txt');
f2= strcat(network1, '/' , corrType, '/129_1/occipital_parietal_129_1.txt');
f3= strcat(network1, '/' , corrType, '/129_1/occipital_leftTemporal_129_1.txt');
f4= strcat(network1, '/' , corrType, '/129_1/occipital_rightTemporal_129_1.txt');
f5= strcat(network1, '/' , corrType, '/129_1/frontal_parietal_129_1.txt');
f6= strcat(network1, '/' , corrType, '/129_1/frontal_leftTemporal_129_1.txt');
f7= strcat(network1, '/' , corrType, '/129_1/frontal_rightTemporal_129_1.txt');
f8= strcat(network1, '/' , corrType, '/129_1/parietal_leftTemporal_129_1.txt');
f9= strcat(network1, '/' , corrType, '/129_1/parietal_rightTemporal_129_1.txt');
f10= strcat(network1, '/' , corrType,  '/129_1/leftTemporal_rightTemporal_129_1.txt');

F1 = dlmread(f1);
F2 = dlmread(f2);
F3 = dlmread(f3);
F4 = dlmread(f4);
F5 = dlmread(f5);
F6 = dlmread(f6);
F7 = dlmread(f7);
F8 = dlmread(f8);
F9 = dlmread(f9);
F10 = dlmread(f10);


subplot(2,5, 1);
imagesc(F1);
title('O-F')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 2);
imagesc(F2);
title('O-P')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 3);
imagesc(F3);
title('O-LT')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 4);
imagesc(F4);
title('O-RT')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 5);
imagesc(F5);
title('F-P')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 6);
imagesc(F6);
title('F-LT')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 7);
imagesc(F7);
title('F-RT')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 8);
imagesc(F8);
title('P-LT')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 9);
imagesc(F9);
title('P-RT')
caxis([-20, 30]);
colormap jet
colorbar
hold on
subplot(2,5, 10);
imagesc(F10);
title('LT-RT')
caxis([-20, 30]);
colormap jet
colorbar
hold on