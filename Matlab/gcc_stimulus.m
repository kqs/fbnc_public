corrType = '/';
for i = 1:4
    
   network1 = 'd:/Data/Results/metrics/network_pearson/';
   %s1 = fscanf(fopen(strcat(network1,  '/', corrType, '/s', int2str(i),'/gcc_129_1.txt'), 'r'), '%f');
   %s2 = fscanf(fopen(strcat(network1,  '/', corrType, '/s', int2str(i),'/gcc_129_2.txt'), 'r'), '%f');
   %s3 = fscanf(fopen(strcat(network1,  corrType, '/s', int2str(i), '/','/gcc_129_3.txt'), 'r'), '%f');
   s1 = fscanf(fopen(strcat(network1, '/s', int2str(i),  '/', corrType,'/gcc_129_1.txt'), 'r'), '%f');
   s2 = fscanf(fopen(strcat(network1, '/s', int2str(i), '/', corrType, '/gcc_129_2.txt'), 'r'), '%f');
   s3 = fscanf(fopen(strcat(network1,  '/s', int2str(i), '/', corrType,'/gcc_129_3.txt'), 'r'), '%f');
   
   s11 = mean(s1);
   s22 = mean(s2);
   s33 = mean(s3);
   
   subplot(1, 4, i);
   bar([s11 s22 s33]);    
end