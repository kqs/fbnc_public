closeness = fscanf(fopen(strcat('d:/Data/Results/metrics/r/network_peaks/CC/s2/bc_129_1.txt'),'r'), '%f');
closeness2 = fscanf(fopen(strcat('d:/Data/Results/metrics/r/network_peaks/CC/s2/bc_129_2.txt'),'r'), '%f');
closeness3 = fscanf(fopen(strcat('d:/Data/Results/metrics/r/network_peaks/CC/s2/bc_129_3.txt'),'r'), '%f');

x = (1:128); 

p1 = stem(closeness);
p1.Marker = 'o';
p1.Color = 'blue';
p1.MarkerFaceColor = 'blue';
hold on;
p2 = stem(closeness2);
p2.Marker = 'd';
p2.Color = 'red';
p2.MarkerFaceColor = 'red';
hold on;
p3 = stem(closeness3);
p3.Marker = 's';
p3.Color = 'black';
p3.MarkerFaceColor = 'black';

ylabel('Betweenness Centrality')
xlabel('Channels')
legend({'Seen','Uncertain', 'Unseen'},'Location','northwest')
xtickangle(90)
set(gca,'XTick',x );
axis([0 inf 0 inf])