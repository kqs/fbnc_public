s1_lags_CC = 'D:/Data/Results/metrics/avg_path_length_v1/apl_pearson_s1.txt';
s2_lags_CC = 'D:/Data/Results/metrics/avg_path_length_v1/apl_pearson_s2.txt';
s3_lags_CC = 'D:/Data/Results/metrics/avg_path_length_v1/apl_pearson_s3.txt';
s4_lags_CC = 'D:/Data/Results/metrics/avg_path_length_v1/apl_pearson_s4.txt';

 s1_lags_SC = 'D:/Data/Results/metrics/avg_path_length_v1/apl_lags_s1_SC.txt';
 s2_lags_SC = 'D:/Data/Results/metrics/avg_path_length_v1/apl_lags_s2_SC.txt';
 s3_lags_SC = 'D:/Data/Results/metrics/avg_path_length_v1/apl_lags_s3_SC.txt';
 s4_lags_SC = 'D:/Data/Results/metrics/avg_path_length_v1/apl_lags_s4_SC.txt';

r1 = fscanf(fopen(s1_lags_CC,'r'), '%f');
r2 = fscanf(fopen(s2_lags_CC,'r'), '%f');
r3 = fscanf(fopen(s3_lags_CC,'r'), '%f');
r4 = fscanf(fopen(s4_lags_CC,'r'), '%f');

r5 = fscanf(fopen(s1_lags_SC,'r'), '%f');
r6 = fscanf(fopen(s2_lags_SC,'r'), '%f');
r7 = fscanf(fopen(s3_lags_SC,'r'), '%f');
r8 = fscanf(fopen(s4_lags_SC,'r'), '%f');

datas = [r1, r2, r3, r4];
datas2 = [r5, r6, r7, r8];

for n = 1:4
    subplot(2, 4, n);
    x = [1 2 3];
    temp_high = [datas(1, n) datas(2, n) datas(3, n)]; 
    w1 = 0.5; 
    bar(x,temp_high,w1,'FaceColor',[1 0 0])
    hold on
    temp_low = [datas(4, n) datas(5, n) datas(6, n)];
    w2 = 0.3;
    bar(x,temp_low,w2,'FaceColor',[0 0 0])
    hold off
    grid on
    ax = gca;
    ax.XTickLabels = {'SEEN','UNCERTAIN','NOT SEEN'};
    ylabel('Average Path Length')
    title(strcat('Subject ', string(n)));
    legend({'Stimulus','Baseline'},'Location','northwest')
end
% 
for n = 1:4
    subplot(2, 4, n+4);
    x = [1 2 3];
    temp_high = [datas2(1, n) datas2(2, n) datas2(3, n)]; 
    w1 = 0.5; 
    bar(x,temp_high,w1,'FaceColor',[1 0 0])
    hold on
    temp_low = [datas2(4, n) datas2(5, n) datas2(6, n)];
    w2 = 0.3;
    bar(x,temp_low,w2,'FaceColor',[0 0 0])
    hold off
    grid on
    ax = gca;
    ax.XTickLabels = {'SEEN','UNCERTAIN','NOT SEEN'};
    ylabel('Average Path Length')
    title(strcat('Subject ', string(n)));
    legend({'Stimulus','Baseline'},'Location','northwest')
end


%ax.XTickLabelRotation = 45;
