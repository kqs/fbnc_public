stimulus = '129_1'
baseline_stimulus = '150_129_1'
network_name = 'NETWORK_2_LAGS_WEIGHT'

area = 'occipital_frontal_129_1_inlined.txt';
area_baseline = 'occipital_frontal_150_129_inlined.txt'; 
nameoutput = 'CC_OCCIPITAL_FRONTAL_TIMELINE_129_1';
nbTrials = 62;
% 62
% 51
% 94

stimulus_data = []
baseline_data = [];

for i = 0:nbTrials
    index = int2str(i);
    baseline_path = strcat(network_name, '/CC/', baseline_stimulus, '/trial_', index, '/', area_baseline);
    path = strcat(network_name, '/CC/', stimulus, '/trial_', index, '/', area);
    
%     stimulus_data = vertcat(stimulus_data, fscanf(fopen(path,'r'), '%f'));
%     baseline_data = vertcat(baseline_data, fscanf(fopen(baseline_path,'r'), '%f'));

    stimulus_data = [stimulus_data mean(fscanf(fopen(path,'r'), '%f'))];
    baseline_data = [baseline_data mean(fscanf(fopen(baseline_path,'r'), '%f'))];
        

%     subplot(5, 2, i+1);
%     plot(stimulus_data);
%     hold on
%     plot(baseline_data);
    fclose('all');
end
 

% fig = gcf;
% fig.PaperUnits = 'inches';
% fig.PaperPosition = [0 0 100 100];
% print('5by3DimensionsFigure','-dpng','-r0')
h1 = axes;
bar1 = bar(stimulus_data);
set(bar1, 'FaceColor', 'r');

hold on
bar2 = bar(baseline_data);
set(bar2, 'FaceColor', 'g');
set(h1, 'Ydir', 'reverse');
bar2.FaceAlpha = 0.5;
legend('stimulus', 'baseline');

print(nameoutput,'-dpdf','-fillpage')