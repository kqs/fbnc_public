﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Subjects;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;

namespace FBNC_Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private List<SubjectEntry> _subjects;
        private List<Tuple<int, int, int>> _events;
        private List<CorrelationTypeBridge> _correlations;
        private string logFilePath;

        private Subject<string> _logObservable = new Subject<string>();

        public MainWindow()
        {
            InitializeComponent();
            _logObservable.Subscribe(OnNext);
        }

        private void OnNext(string s)
        {
            Dispatcher.Invoke(() =>
            {
                logTextBox.AppendText(s + '\n');
            });
        }

        private void parseSubjectsEventsAndCorrelations()
        {
            _subjects = new List<SubjectEntry>();
            _events = new List<Tuple<int, int, int>>();
            _correlations = new List<CorrelationTypeBridge>();

            foreach (SubjectEntry item in subjectsListView.Items)
            {
                _subjects.Add(item);
            }

            if (event1Checkbox.IsChecked != null && event1Checkbox.IsChecked.Value)
                _events.Add(new Tuple<int, int, int>(129, 1, -1));
            if (event2Checkbox.IsChecked != null && event2Checkbox.IsChecked.Value)
                _events.Add(new Tuple<int, int, int>(129, 2, -1));
            if (event3Checkbox.IsChecked != null && event3Checkbox.IsChecked.Value)
                _events.Add(new Tuple<int, int, int>(129, 3, -1));
            if (event4Checkbox.IsChecked != null && event4Checkbox.IsChecked.Value)
                _events.Add(new Tuple<int, int, int>(150, 129, 1));
            if (event5Checkbox.IsChecked != null && event5Checkbox.IsChecked.Value)
                _events.Add(new Tuple<int, int, int>(150, 129, 2));
            if (event6Checkbox.IsChecked != null && event6Checkbox.IsChecked.Value)
                _events.Add(new Tuple<int, int, int>(150, 129, 3));

            if (crossCorrelationCheckBox.IsChecked != null && crossCorrelationCheckBox.IsChecked.Value)
                _correlations.Add(CorrelationTypeBridge.CROSS_CORRELATION);
            if (scaledCorrelationCheckBox.IsChecked != null && scaledCorrelationCheckBox.IsChecked.Value)
                _correlations.Add(CorrelationTypeBridge.SCALED_CORRELATION);
            if (pearsonCoefficientCheckBox.IsChecked != null && pearsonCoefficientCheckBox.IsChecked.Value)
                _correlations.Add(CorrelationTypeBridge.PEARSON_CORRELATION);
        }

        private void generateButtonClick(object sender, RoutedEventArgs e)
        {
            _logObservable.OnNext("Started at: " + DateTime.Now.ToString("h:mm:ss tt"));
            parseSubjectsEventsAndCorrelations();

            try
            {
                foreach (var subjectEntry in _subjects)
                {
                    if (string.IsNullOrEmpty(subjectEntry.BinaryDataSourcePath) ||
                        string.IsNullOrEmpty(subjectEntry.EventCodesFilePath) ||
                        string.IsNullOrEmpty(subjectEntry.EventTimestampsFilePath) ||
                        string.IsNullOrEmpty(subjectEntry.OutputPath))
                    {
                        MessageBox.Show("Subject name: " + subjectEntry.SubjectName +
                                        " one of the fields are empty!\nThis subject will not be processed!");
                        continue;
                    }

                    new Thread(delegate ()
                    {
                        var bridge = new FunctionalNetworkCreatorBridge(
                            subjectEntry.BinaryDataSourcePath,
                            subjectEntry.EventCodesFilePath,
                            subjectEntry.EventTimestampsFilePath,
                            subjectEntry.OutputPath,
                            subjectEntry.SubjectName,
                            ProgressCallback);

                        foreach (var correlationTypeBridge in _correlations)
                        {
                            foreach (var @event in _events)
                            {
                                bridge.Generate(correlationTypeBridge, @event.Item1, @event.Item2,
                                    @event.Item3);
                            }
                        }
                    }).Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }
        }

        private void ProgressCallback(string str)
        {
            _logObservable.OnNext(str);
        }

        private void addSubjectBtnClick(object sender, System.Windows.RoutedEventArgs e)
        {
            subjectsListView.Items.Add(new SubjectEntry());
        }

        private void browseBinDataBtnClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var listViewitem = getSubjectEntryBasedOnSelection(sender);

            using (var dialog = new OpenFileDialog())
            {
                dialog.ShowDialog();
                var rgx = new Regex(@".Ch[0-9]*");
                listViewitem.BinaryDataSourcePath = rgx.Replace(dialog.FileName, "-Ch%03d");
            }
        }

        private void browseEventCodesDataBtnClick(object sender, RoutedEventArgs e)
        {
            var listViewItem = getSubjectEntryBasedOnSelection(sender);
            listViewItem.EventCodesFilePath = popupBrowseDialog();
        }

        private void browseEventTimestampsDataBtnClick(object sender, RoutedEventArgs e)
        {
            var listViewItem = getSubjectEntryBasedOnSelection(sender);
            listViewItem.EventTimestampsFilePath = popupBrowseDialog();
        }

        private void browseOutputPathBtn(object sender, RoutedEventArgs e)
        {
            var listViewItem = getSubjectEntryBasedOnSelection(sender);
            using (var dialog = new FolderBrowserDialog())
            {
                dialog.ShowDialog();
                listViewItem.OutputPath = dialog.SelectedPath;
            }
        }

        private SubjectEntry getSubjectEntryBasedOnSelection(object sender)
        {
            var button = (sender as FrameworkElement).DataContext;
            var index = subjectsListView.Items.IndexOf(button);

            return (SubjectEntry)subjectsListView.Items.GetItemAt(index);
        }

        private string popupBrowseDialog()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.ShowDialog();
                return dialog.FileName;
            }
        }

        private void deleteSubjectBtnClick(object sender, RoutedEventArgs e)
        {
            var button = (sender as FrameworkElement).DataContext;
            var index = subjectsListView.Items.IndexOf(button);
            subjectsListView.Items.RemoveAt(index);
        }

        private void browseFilesBtnClick(object sender, RoutedEventArgs e)
        {
            var selectedFileNames = new List<string>();
            using (var dialog = new OpenFileDialog())
            {
                dialog.Multiselect = true;
                dialog.ShowDialog();
                selectedFileNames.AddRange(dialog.FileNames);
            }

            selectedFileNamesListBox.Items.Clear();
            foreach (var fileName in selectedFileNames)
            {
                selectedFileNamesListBox.Items.Add(fileName);
            }
        }

        private void browseOutputAvgFIleBtn_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.ShowDialog();
                outputAvgTrialFileTextBox.Text = dialog.FileName;
            }
        }

        private void createAvgTrialBtn_Click(object sender, RoutedEventArgs e)
        {
            if (browseFolderToConvertTextBox.Text.Length != 0)
            {
                var rootDir = new DirectoryInfo(browseFolderToConvertTextBox.Text);
                createAvgNetworkForSubDirs(rootDir);
            }
            else
            {
                generateAvgNetwork(selectedFileNamesListBox.Items.OfType<string>().ToList(),
                    outputAvgTrialFileTextBox.Text);
            }
        }

        private void browseFolderBtn_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                dialog.ShowDialog();
                browseFolderToConvertTextBox.Text = dialog.SelectedPath;
            }
        }

        private void createAvgNetworkForSubDirs(DirectoryInfo dir)
        {
            if (dir.GetDirectories().Length == 0)
            {
                var files = dir.GetFiles();
                generateAvgNetwork(files.Select(x => x.FullName).ToList(), dir.FullName + "/avg.txt");
            }
            else
            {
                foreach (var directoryInfo in dir.GetDirectories())
                {
                    createAvgNetworkForSubDirs(directoryInfo);
                }
            }
        }

        private void generateAvgNetwork(IEnumerable<object> filePaths, string outputFile)
        {
            float[,] meanMatrix = new float[128,128];
            int i, j;
            foreach (string fileNamePath in filePaths)
            {
                i = 0;
                var matrixContent = File.ReadAllText(fileNamePath);
                foreach (var line in matrixContent.Split('\n'))
                {
                    j = 0;
                    foreach (var element in line.Split(' '))
                    {
                        if (i == 128 || j == 128) break;

                        meanMatrix[i, j] += float.Parse(element);
                        ++j;
                    }
                    ++i;
                }
            }

            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }
            else
            {
                var fs = File.Create(outputFile);
                fs.Close();
            }

            using (var textWriter = new StreamWriter(outputFile))
            {
                for (i = 0; i < 128; ++i)
                {
                    for (j = 0; j < 128; ++j)
                    {
                        meanMatrix[i, j] /= selectedFileNamesListBox.Items.Count;
                        textWriter.Write(meanMatrix[i, j] + " ");
                    }
                    textWriter.WriteLine();
                }
            }
        }

        private void browseFilesAvgPathLengthBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectedFileNames = new List<string>();
            using (var dialog = new OpenFileDialog())
            {
                dialog.Multiselect = true;
                dialog.ShowDialog();
                selectedFileNames.AddRange(dialog.FileNames);
            }

            if (appendBrowsedFilesToList.IsChecked != null && !appendBrowsedFilesToList.IsChecked.Value)
                avgPathLengthListBox.Items.Clear();
            foreach (var fileName in selectedFileNames)
            {
                avgPathLengthListBox.Items.Add(fileName);
            }
        }

        private void computeAvgPathLengthBtn_Click(object sender, RoutedEventArgs e)
        {
            var metricsAnalyzer = new MetricsAnalyzer();

            var averagePathLengths = metricsAnalyzer.GetAveragePathLength(avgPathLengthListBox.Items.OfType<string>().ToArray());

            avgPathLengthTextBoxResults.Text = "";
            foreach (var avgPathLength in averagePathLengths)   
            {
                avgPathLengthTextBoxResults.AppendText(avgPathLength  + "\n");
            }
        }

        private void resetInputAvgPathBtn_Click(object sender, RoutedEventArgs e)
        {
            avgPathLengthListBox.Items.Clear();
            avgPathLengthTextBoxResults.Text = "";
        }

        private void browseCommunity_Click(object sender, RoutedEventArgs e)
        {
            networkFileTextBox.Text = popupBrowseDialog();
        }

        private void ReduceNetworkDensity(string fileNamePath, float density)
        {
            var matrix = Utils.ReadMatrixFromFile(fileNamePath, 128);
            var edgeListTuple = new List<Tuple<int, int, float>>();
            for (var i = 0; i < 128; ++i)
            {
                for (var j = i + 1; j < 128; ++j)
                {
                    edgeListTuple.Add(new Tuple<int, int, float>(i, j, matrix[i, j]));
                }
            }

            edgeListTuple.Sort((tuple, tuple1) => tuple1.Item3.CompareTo(tuple.Item3));

            var newMatrix = new float[128, 128];
            for (var i = 0; i < edgeListTuple.Count * density; ++i)
            {
                newMatrix[edgeListTuple[i].Item1, edgeListTuple[i].Item2] = edgeListTuple[i].Item3;
                newMatrix[edgeListTuple[i].Item2, edgeListTuple[i].Item1] = edgeListTuple[i].Item3;
            }

            Utils.WriteMatrixToFile(fileNamePath, newMatrix);
        }

        private void generateCommunity_Click(object sender, RoutedEventArgs e)
        {
            float density;
            if (!float.TryParse(networkDensity.Text, out density) || (density < 0 || density > 100))
            {
                MessageBox.Show("Network density must be in [0-100] interval!");
                return;
            }

            var networkFilePath = networkFileTextBox.Text;
            var tempNetworkFilePath = $"{Path.GetDirectoryName(networkFilePath)}\\{Path.GetFileNameWithoutExtension(networkFilePath)}.adj";
            File.Copy(networkFilePath, tempNetworkFilePath, true);
            ReduceNetworkDensity(tempNetworkFilePath, float.Parse(networkDensity.Text)/100.0f);

            var pacoExeFile = pacoExeFilePath.Text;
            var unityPlayerFile = unityFilepath.Text;
            var pacoExeFileDir = Path.GetDirectoryName(pacoExeFile);

            var membershipFilePath = $"{pacoExeFileDir}\\membership.txt";
            var communitiesMatrixPath = $"{pacoExeFileDir}\\membership_matrix.txt";

            var proc = new Process();
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.FileName = pacoExeFile;
            proc.StartInfo.Arguments = $"-q 2 {tempNetworkFilePath}";
            proc.StartInfo.WorkingDirectory = pacoExeFileDir;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;

            proc.Start();

            var output = proc.StandardOutput.ReadToEnd();
            var error = proc.StandardError.ReadToEnd();
            
            proc.WaitForExit();
            
            if (!string.IsNullOrEmpty(error))
            {
                MessageBox.Show("Error: " + error);
                return;
            }

            MessageBox.Show("Asymptotic Surprize: " + output);

            var membershipFile = File.ReadAllLines(membershipFilePath);
            var communities = Utils.FormatCommunities(membershipFile);

            var matrix = Utils.ReadMatrixFromFile(networkFilePath, 128);

            var communitiesMatrix = new float[128,128];
            foreach (var community in communities)
            {
                for (var i = 0; i < community.Value.Count; ++i)
                {
                    for (var j = i + 1; j < community.Value.Count; ++j)
                    {
                        int firstNode = community.Value.ElementAt(i);
                        int secondNode = community.Value.ElementAt(j);
                        if (matrix[firstNode, secondNode] != 0)
                            communitiesMatrix[firstNode, secondNode] = matrix[firstNode, secondNode];
                        if (matrix[secondNode, firstNode] != 0)
                            communitiesMatrix[secondNode, firstNode] = matrix[secondNode, firstNode];
                    }
                }
            }

            using (var streamWriter = new StreamWriter(communitiesMatrixPath))
            {
                for (var i = 0; i < 128; i++)
                {
                    for (var j = 0; j < 128; ++j)
                        streamWriter.Write(communitiesMatrix[i,j] + " ");
                    streamWriter.WriteLine();
                }
            }

            //Launch viewer
            proc.StartInfo.FileName = unityPlayerFile;
            proc.StartInfo.Arguments = $"1 {membershipFilePath} {communitiesMatrixPath}";
            proc.Start();
            proc.WaitForExit();
        }

        private void pacoBrowseFilePath_Click(object sender, RoutedEventArgs e)
        {
            pacoExeFilePath.Text = popupBrowseDialog();
        }

        private void unityBrowseFilePath_Click(object sender, RoutedEventArgs e)
        {
            unityFilepath.Text = popupBrowseDialog();
        }

        private void viewFile_Click(object sender, RoutedEventArgs e)
        {
            var networkPath = networkFileTextBox.Text;
            var unityPlayerFile = unityFilepath.Text;

            var proc = new Process();
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.FileName = unityPlayerFile;
            proc.StartInfo.Arguments = $"0 {networkPath}";
            proc.Start();
            proc.WaitForExit();
        }
    }
}
