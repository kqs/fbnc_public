﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FBNC_Desktop
{
    public static class Utils
    {
        public static Dictionary<int, List<int>> FormatCommunities(string[] lines)
        {
            var comms = new Dictionary<int, List<int>>();
            for (var i = 0; i < lines.Length; ++i)
            {
                int communityNb;
                if (!int.TryParse(lines[i], out communityNb)) continue;

                List<int> communityElements;
                if (!comms.TryGetValue(communityNb, out communityElements))
                {
                    communityElements = new List<int>(new[] { i });
                    comms.Add(communityNb, communityElements);
                }
                else
                {
                    communityElements.Add(i);
                }
            }

            return comms;
        }

        public static void WriteMatrixToFile(string file, float[,] matrix)
        {
            using (var streamWriter = new StreamWriter(file, false))
            {
                for (var i = 0; i < 128; ++i)
                {
                    for (var j = 0; j < 128; ++j)
                    {
                        streamWriter.Write(matrix[i, j] + " ");
                    }
                    streamWriter.Write("\n");
                }
            }
        }

        public static float[,] ReadMatrixFromFile(string file, int size)
        {
            var matrix = new float[size, size];
            int i, j;

            i = 0;
            var matrixContent = File.ReadAllText(file);
            foreach (var line in matrixContent.Split('\n'))
            {
                j = 0;
                foreach (var element in line.Split(' '))
                {
                    if (i == size || j == size) break;

                    matrix[i, j] = float.Parse(element);
                    ++j;
                }
                ++i;
            }

            return matrix;
        }
    }
}
