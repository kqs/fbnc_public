﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FBNC_Desktop
{
    public class AveragePathLength
    {
        public float ComputeAveragePathLength(float[,] matrix, int n, List<int> nodesToIgnore)
        {
            float shortestPathSum = 0;

            for (var i = 0; i < n; ++i)
            {
                if (nodesToIgnore.Contains(i)) continue;

                var distances = computeBellmanFord(matrix, n, i);

                for (var j = 0; j < distances.Length; ++j)
                {
                    shortestPathSum += distances[j];
                }
            }

            int realNodes = n - nodesToIgnore.Count;
            return 1.0f / (realNodes * (realNodes - 1)) * shortestPathSum;
        }

        private float[] computeBellmanFord(float[,] matrix, int size, int source)
        {
            float[] distances = Enumerable.Repeat(float.MaxValue, size).ToArray();
            distances[source] = 0;

            for (var i = 1; i <= size - 1; ++i)
            {
                for (var x1 = 0; x1 < size; ++x1)
                {
                    for (var x2 = 0; x2 < size; ++x2)
                    {
                        var weight = matrix[x1,x2];

                        if (weight == 0) continue;
                        if (weight < 0)
                            weight = Math.Abs(weight);

                        if (distances[x1] + weight < distances[x2] && distances[x1] != float.MaxValue)
                            distances[x2] = distances[x1] + weight;
                    }
                }
            }

            for (var i = 0; i < size; ++i)
            {
                if (distances[i] == float.MaxValue)
                    distances[i] = 0;
            }

            return distances;
        }
    }
}
