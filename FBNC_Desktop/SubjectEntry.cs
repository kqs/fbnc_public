﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using FBNC_Desktop.Annotations;

namespace FBNC_Desktop
{
    public class SubjectEntry : INotifyPropertyChanged
    {
        private string _binaryDataSourcePath;
        private string _eventCodesFilePath;
        private string _eventTimestampsFilePath;
        private string _outputPath;
        private string _subjectName;

        public SubjectEntry()
        {
        }

        public SubjectEntry(string binaryDataSourcePath, string eventCodesFilePath, string eventTimestampsFilePath, string outputPath, string subjectName)
        {
            BinaryDataSourcePath = binaryDataSourcePath;
            EventCodesFilePath = eventCodesFilePath;
            EventTimestampsFilePath = eventTimestampsFilePath;
            OutputPath = outputPath;
            SubjectName = subjectName;
        }

        public string BinaryDataSourcePath
        {
            get { return _binaryDataSourcePath; }
            set
            {
                _binaryDataSourcePath = value;
                OnPropertyChanged(nameof(BinaryDataSourcePath));
            }
        }

        public string EventCodesFilePath
        {
            get { return _eventCodesFilePath; }
            set
            {
                _eventCodesFilePath = value;
                OnPropertyChanged(nameof(EventCodesFilePath));
            }
        }

        public string EventTimestampsFilePath
        {
            get { return _eventTimestampsFilePath; }
            set
            {
                _eventTimestampsFilePath = value;
                OnPropertyChanged(nameof(EventTimestampsFilePath));
            }
        }

        public string OutputPath
        {
            get { return _outputPath; }
            set
            {
                _outputPath = value;
                OnPropertyChanged(nameof(OutputPath));
            }
        }

        public string SubjectName
        {
            get { return _subjectName; }
            set
            {
                _subjectName = value;
                OnPropertyChanged(nameof(SubjectName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
